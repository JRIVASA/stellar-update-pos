DECLARE @ErrorVar INT

SET @ErrorVar = 0

USE [master]

-- SET REMOTE CONNECTION PROPERTIES

IF EXISTS(SELECT * FROM SYS.SERVERS WHERE Name = N'$(SERVERNAME)')
BEGIN
	EXEC master.sys.sp_dropserver '$(SERVERNAME)','droplogins'  
END

EXEC master.dbo.sp_addlinkedserver @server = N'$(SERVERNAME)', @srvproduct=N'SQL Server'

EXEC master.dbo.sp_serveroption @server=N'$(SERVERNAME)', @optname=N'collation compatible', @optvalue=N'false'

EXEC master.dbo.sp_serveroption @server=N'$(SERVERNAME)', @optname=N'data access', @optvalue=N'true'

EXEC master.dbo.sp_serveroption @server=N'$(SERVERNAME)', @optname=N'dist', @optvalue=N'false'

EXEC master.dbo.sp_serveroption @server=N'$(SERVERNAME)', @optname=N'pub', @optvalue=N'false'

EXEC master.dbo.sp_serveroption @server=N'$(SERVERNAME)', @optname=N'rpc', @optvalue=N'false'

EXEC master.dbo.sp_serveroption @server=N'$(SERVERNAME)', @optname=N'rpc out', @optvalue=N'false'

EXEC master.dbo.sp_serveroption @server=N'$(SERVERNAME)', @optname=N'sub', @optvalue=N'false'

EXEC master.dbo.sp_serveroption @server=N'$(SERVERNAME)', @optname=N'connect timeout', @optvalue=N'0'

EXEC master.dbo.sp_serveroption @server=N'$(SERVERNAME)', @optname=N'collation name', @optvalue=null

EXEC master.dbo.sp_serveroption @server=N'$(SERVERNAME)', @optname=N'lazy schema validation', @optvalue=N'false'

EXEC master.dbo.sp_serveroption @server=N'$(SERVERNAME)', @optname=N'query timeout', @optvalue=N'0'

EXEC master.dbo.sp_serveroption @server=N'$(SERVERNAME)', @optname=N'use remote collation', @optvalue=N'true'

EXEC master.dbo.sp_serveroption @server=N'$(SERVERNAME)', @optname=N'remote proc transaction promotion', @optvalue=N'true'

EXEC master.dbo.sp_addlinkedsrvlogin @rmtsrvname = N'$(SERVERNAME)', @locallogin = NULL , @useself = N'False', @rmtuser = N'$(LNAME)', @rmtpassword = N'$(KEY)'

------------------------------

SET @ErrorVar = @@ERROR
PRINT 'ERROR ? => ' + CAST(@ErrorVar AS NVARCHAR(MAX))

GO -- SEPARAR PROCEDIMIENTOS. EL LINKED SERVER DEBE ESTAR ESTABLECIDO PREVIAMENTE ANTES
--DE EJECUTAR EL RESTO DE LAS INSTRUCCIONES

-------------------------------------------------

DECLARE @ErrorVar INT
DECLARE @TmpQuery NVARCHAR(MAX)

SET @ErrorVar = 0

DECLARE @MASTER_CONNECTED INT
DECLARE @VAD10_CONNECTED INT
DECLARE @VAD20_CONNECTED INT

-----------------------

SET @ErrorVar = @@ERROR
PRINT 'ERROR ? => ' + CAST(@ErrorVar AS NVARCHAR(MAX))

SELECT @MASTER_CONNECTED = 1 FROM [$(SERVERNAME)].[MASTER].[DBO].[SPT_MONITOR]
SELECT @VAD10_CONNECTED = 1 FROM [$(SERVERNAME)].[VAD10].[DBO].[ESTRUC_SIS]
SELECT @VAD20_CONNECTED = 1 FROM [$(SERVERNAME)].[VAD20].[DBO].[MA_CAJA]

IF (@MASTER_CONNECTED = 1 AND @VAD10_CONNECTED = 1 AND @VAD20_CONNECTED = 1)
BEGIN

USE [ADM_LOCAL_FOOD]

BEGIN TRANSACTION

TRUNCATE TABLE [MA_BANCOS]

TRUNCATE TABLE [MA_CLIENTES]

TRUNCATE TABLE [MA_DENOMINACIONES]

TRUNCATE TABLE [MA_DEPOSITO]

TRUNCATE TABLE [MA_MONEDAS]

TRUNCATE TABLE [MA_USUARIOS]

INSERT INTO [MA_BANCOS] (c_CODIGO,
c_DESCRIPCIO,
c_GRUPO,
c_OBSERVACIO,
N_IDB,
bs_verificacionoperaciones_especiales,
cs_mensaje,
ns_codigooperacion,
cs_rtfcheque,
cs_codigobanco_che)
SELECT c_CODIGO,
c_DESCRIPCIO,
c_GRUPO,
c_OBSERVACIO,
N_IDB,
bs_verificacionoperaciones_especiales,
cs_mensaje,
ns_codigooperacion,
cs_rtfcheque,
cs_codigobanco_che FROM [$(SERVERNAME)].[VAD10].[DBO].[MA_BANCOS]

INSERT INTO [MA_CLIENTES] (c_CODCLIENTE,
c_DESCRIPCIO,
c_GRUPO,
c_RIF,
C_NIT,
c_REPRESENTA,
c_CARGO,
c_DIRECCION,
c_CIUDAD,
c_ESTADO,
c_PAIS,
c_EMAIL,
c_TELEFONO,
c_FAX,
c_WEB,
n_LIMITE,
n_DIAS,
N_PRECIO,
n_DESCUENTO,
c_OBSERVACIO,
c_impuesto,
n_activo,
update_date,
add_date,
f_cumpleanos,
N_TIPO,
C_CODCLIENTE_REL,
N_RETENCION,
nu_tipodescuento,
N_DiasIntMora,
N_InteresesMora,
c_fileimagen,
NU_PUNTOSACUMULADOS,
RESTRINGIDO,
CS_COMPRADOR,
cs_codfrecuente,
bs_contribuyente )
SELECT c_CODCLIENTE,
c_DESCRIPCIO,
c_GRUPO,
c_RIF,
C_NIT,
c_REPRESENTA,
c_CARGO,
c_DIRECCION,
c_CIUDAD,
c_ESTADO,
c_PAIS,
c_EMAIL,
c_TELEFONO,
c_FAX,
c_WEB,
n_LIMITE,
n_DIAS,
N_PRECIO,
n_DESCUENTO,
c_OBSERVACIO,
c_impuesto,
n_activo,
update_date,
add_date,
f_cumpleanos,
N_TIPO,
C_CODCLIENTE_REL,
N_RETENCION,
nu_tipodescuento,
N_DiasIntMora,
N_InteresesMora,
c_fileimagen,
NU_PUNTOSACUMULADOS,
RESTRINGIDO,
CS_COMPRADOR,
cs_codfrecuente,
bs_contribuyente FROM [$(SERVERNAME)].[VAD10].[DBO].[MA_CLIENTES]

INSERT INTO [MA_DENOMINACIONES] (c_codmoneda,
c_coddenomina,
c_denominacion,
n_valor,
c_real,
c_pos,
n_monto_compra,
n_monto_vuelto,
b_permite_vuelto,
nu_requiere_endoso,
nu_imprime_forma,
nu_requiere_conformacion,
nu_requiere_serial,
nu_verificacionElectronica,
bs_verificacionoperaciones_especiales,
bs_credito )
SELECT c_codmoneda,
c_coddenomina,
c_denominacion,
n_valor,
c_real,
c_pos,
n_monto_compra,
n_monto_vuelto,
b_permite_vuelto,
nu_requiere_endoso,
nu_imprime_forma,
nu_requiere_conformacion,
nu_requiere_serial,
nu_verificacionElectronica,
bs_verificacionoperaciones_especiales,
bs_credito FROM [$(SERVERNAME)].[VAD10].[DBO].[MA_DENOMINACIONES]

INSERT INTO [MA_DEPOSITO] (c_coddeposito,
c_descripcion,
c_codlocalidad,
c_responsable,
c_observacion,
update_date,
add_date,
cu_mascara)
SELECT c_coddeposito,
c_descripcion,
c_codlocalidad,
c_responsable,
c_observacion,
update_date,
add_date,
cu_mascara FROM [$(SERVERNAME)].[VAD10].[DBO].[MA_DEPOSITO]

INSERT INTO [MA_MONEDAS] (c_codmoneda,
c_descripcion,
n_factor,
b_preferencia,
c_observacio,
b_activa,
c_simbolo,
n_decimales )
SELECT c_codmoneda,
c_descripcion,
n_factor,
b_preferencia,
c_observacio,
b_activa,
c_simbolo,
n_decimales FROM [$(SERVERNAME)].[VAD10].[DBO].[MA_MONEDAS]

INSERT INTO [MA_USUARIOS] (codusuario,
[password],
login_name,
descripcion,
clave,
localidad,
Nivel,
Vendedor,
add_date,
update_date,
tipo_usuario,
MOD_PRECIO_VENTA,
MOD_COSTO_COMPRA,
BU_CAMBIA_CLAVE )
SELECT codusuario,
[password],
login_name,
descripcion,
clave,
localidad,
Nivel,
Vendedor,
add_date,
update_date,
tipo_usuario,
MOD_PRECIO_VENTA,
MOD_COSTO_COMPRA,
BU_CAMBIA_CLAVE FROM [$(SERVERNAME)].[VAD10].[DBO].[MA_USUARIOS]

IF EXISTS(SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'MA_PRODxPROV')
BEGIN
	
	SET @TmpQuery = 'TRUNCATE TABLE [MA_PRODxPROV]'
	EXEC (@TmpQuery)

	SET @TmpQuery = 'INSERT INTO [MA_PRODxPROV] (c_Codigo, 
	c_CodProvee, 
	c_Numero_Compra, 
	d_Fecha, 
	n_Costo, 
	Add_Date, 
	Update_Date, 
	n_Prod_Ext, 
	b_Preferencial, 
	Estatus)
	SELECT c_Codigo, 
	c_CodProvee, 
	c_Numero_Compra, 
	d_Fecha, 
	n_Costo, 
	Add_Date, 
	Update_Date, 
	n_Prod_Ext, 
	b_Preferencial, 
	Estatus FROM [$(SERVERNAME)].[VAD10].[DBO].[MA_PRODxPROV]'

	EXEC (@TmpQuery)

END

-------- OPCIONALES --------

IF EXISTS(SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'MA_CAPTAHUELLAS_PERFILES')
BEGIN
	
	DECLARE @TmpColsCPT1 NVARCHAR(MAX)
	DECLARE @TmpColsCPT2 NVARCHAR(MAX)
	DECLARE @TmpColsCPT3 NVARCHAR(MAX)

	SET @TmpColsCPT1 = (SELECT 'MA_CAPTAHUELLAS_PERFILES.' + COLUMN_NAME + ',' AS 'data()'
	FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'MA_CAPTAHUELLAS_PERFILES'
	AND COLUMN_NAME NOT IN ('ID')
	FOR XML PATH (''))
	SET @TmpColsCPT1 = SUBSTRING(@TmpColsCPT1, 1, LEN(@TmpColsCPT1) - 1)

	SET @TmpColsCPT2 = (SELECT 'MA_CAPTAHUELLAS_USUARIOS.' + COLUMN_NAME + ',' AS 'data()'
	FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'MA_CAPTAHUELLAS_USUARIOS'
	AND COLUMN_NAME NOT IN ('ID')
	FOR XML PATH (''))
	SET @TmpColsCPT2 = SUBSTRING(@TmpColsCPT2, 1, LEN(@TmpColsCPT2) - 1)

	SET @TmpColsCPT3 = (SELECT 'TR_CAPTAHUELLAS_PERFILES.' + COLUMN_NAME + ',' AS 'data()'
	FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'TR_CAPTAHUELLAS_PERFILES'
	AND COLUMN_NAME NOT IN ('ID')
	FOR XML PATH (''))
	SET @TmpColsCPT3 = SUBSTRING(@TmpColsCPT3, 1, LEN(@TmpColsCPT3) - 1)

	SET @TmpQuery = 'TRUNCATE TABLE [MA_CAPTAHUELLAS_PERFILES]'
	EXEC (@TmpQuery)
	SET @TmpQuery = 'TRUNCATE TABLE [MA_CAPTAHUELLAS_USUARIOS]'
	EXEC (@TmpQuery)
	SET @TmpQuery = 'TRUNCATE TABLE [TR_CAPTAHUELLAS_PERFILES]'
	EXEC (@TmpQuery)

	SET @TmpQuery = 'INSERT INTO [MA_CAPTAHUELLAS_PERFILES] (' + REPLACE(@TmpColsCPT1 COLLATE Modern_Spanish_CI_AS, 'MA_CAPTAHUELLAS_PERFILES.', '') + ')' + CHAR(13) + 
	'SELECT ' + REPLACE(@TmpColsCPT1 COLLATE Modern_Spanish_CI_AS, 'MA_CAPTAHUELLAS_PERFILES.', '') + ' FROM [$(SERVERNAME)].[VAD10].[DBO].[MA_CAPTAHUELLAS_PERFILES]' + CHAR(13) + 
	'WHERE 1 = 1'
	EXEC (@TmpQuery)

	SET @TmpQuery = 'INSERT INTO [MA_CAPTAHUELLAS_USUARIOS] (' + REPLACE(@TmpColsCPT2 COLLATE Modern_Spanish_CI_AS, 'MA_CAPTAHUELLAS_USUARIOS.', '') + ')' + CHAR(13) + 
	'SELECT ' + REPLACE(@TmpColsCPT2 COLLATE Modern_Spanish_CI_AS, 'MA_CAPTAHUELLAS_USUARIOS.', '') + ' FROM [$(SERVERNAME)].[VAD10].[DBO].[MA_CAPTAHUELLAS_USUARIOS]' + CHAR(13) + 
	'WHERE 1 = 1'
	EXEC (@TmpQuery)

	SET @TmpQuery = 'INSERT INTO [TR_CAPTAHUELLAS_PERFILES] (' + REPLACE(@TmpColsCPT3 COLLATE Modern_Spanish_CI_AS, 'TR_CAPTAHUELLAS_PERFILES.', '') + ')' + CHAR(13) + 
	'SELECT ' + REPLACE(@TmpColsCPT3 COLLATE Modern_Spanish_CI_AS, 'TR_CAPTAHUELLAS_PERFILES.', '') + ' FROM [$(SERVERNAME)].[VAD10].[DBO].[TR_CAPTAHUELLAS_PERFILES]' + CHAR(13) + 
	'WHERE 1 = 1'
	EXEC (@TmpQuery)

END

-------- FIN OPCIONALES --------

COMMIT TRANSACTION

USE [POS_LOCAL_FOOD]

BEGIN TRANSACTION

TRUNCATE TABLE [MA_CODIGOS]

TRUNCATE TABLE [MA_ETIQUETAS]

TRUNCATE TABLE [MA_PRODUCTOS]

TRUNCATE TABLE [MA_SORTEOS]

TRUNCATE TABLE [MA_VENTAS_VOLUMEN]

DECLARE @ColumnExists BIT

INSERT INTO [MA_CODIGOS] (c_codnasa,
c_codigo,
c_descripcion,
n_cantidad,
nu_intercambio,
nu_tipoprecio )
SELECT c_codnasa,
c_codigo,
c_descripcion,
n_cantidad,
nu_intercambio,
nu_tipoprecio FROM [$(SERVERNAME)].[VAD20].[DBO].[MA_CODIGOS]

SET @TmpQuery = '
INSERT INTO [MA_ETIQUETAS] (C_CODIGO,
C_DESCRIPCIO,
C_LONGITUD,
N_LONGITUD,
N_POS_INI_1,
N_CARACTERES_1,
C_CAMPO,
N_POS_INI_2,
N_CARACTERES_2,
B_MANEJA_DECIMAL,
N_POS_INI_3,
N_CARACTERES_3,
B_ACTIVA'

IF EXISTS(SELECT * FROM [$(SERVERNAME)].[VAD20].[INFORMATION_SCHEMA].[COLUMNS] WHERE TABLE_NAME = 'MA_ETIQUETAS' AND COLUMN_NAME = 'b_VerificaDigito')
	SET @ColumnExists = 1
ELSE
	SET @ColumnExists = 0

IF (@ColumnExists = 1)
BEGIN
	SET @TmpQuery = @TmpQuery + ', b_VerificaDigito'
END 

SET @TmpQuery = @TmpQuery + ')'

SET @TmpQuery = @TmpQuery + CHAR(13) + '
SELECT C_CODIGO,
C_DESCRIPCIO,
C_LONGITUD,
N_LONGITUD,
N_POS_INI_1,
N_CARACTERES_1,
C_CAMPO,
N_POS_INI_2,
N_CARACTERES_2,
B_MANEJA_DECIMAL,
N_POS_INI_3,
N_CARACTERES_3,
B_ACTIVA'

IF (@ColumnExists = 1)
BEGIN
	SET @TmpQuery = @TmpQuery + ', b_VerificaDigito'
END 

SET @TmpQuery = @TmpQuery + CHAR(13) +
'FROM [$(SERVERNAME)].[VAD20].[DBO].[MA_ETIQUETAS]'

EXEC (@TmpQuery)

INSERT INTO [MA_PRODUCTOS] (C_CODIGO,
C_DESCRI,
c_departamento,
c_grupo,
c_subgrupo,
c_marca,
c_modelo,
c_procede,
n_costoact,
n_costoant,
n_costopro,
n_costorep,
n_precio1,
n_precio2,
n_precio3,
c_seriales,
c_compuesto,
c_presenta,
n_peso,
n_volumen,
n_cantibul,
n_pesobul,
n_volbul,
c_fileimagen,
n_impuesto1,
n_impuesto2,
n_impuesto3,
c_cod_arancel,
c_des_arancel,
n_por_arancel,
n_costo_original,
c_observacio,
n_activo,
n_tipopeso,
n_precioO,
f_inicial,
f_final,
h_inicial,
h_final,
add_date,
update_date,
c_codfabricante,
HABLADOR,
C_CODMONEDA,
CANT_DECIMALES,
MONEDA_ANT,
MONEDA_ACT,
MONEDA_PRO,
C_CODIGO_BASE,
C_DESCRI_BASE,
TEXT1,
TEXT2,
TEXT3,
TEXT4,
TEXT5,
TEXT6,
TEXT7,
TEXT8,
DATE1,
NUME1,
N_CANTIDAD_TMP,
C_COD_PLANTILLA,
c_usuarioAdd,
c_usuarioupd,
N_PROD_EXT,
N_PRO_EXT,
NU_TIPO_PRODUCTO,
nu_insumointerno,
nu_precioregulado,
nu_pocentajemerma,
nu_nivelClave,
CU_DESCRIPCION_CORTA,
bs_permiteteclado,
bs_permitecantidad,
nu_stockmin,
nu_stockmax )
SELECT C_CODIGO,
C_DESCRI,
c_departamento,
c_grupo,
c_subgrupo,
c_marca,
c_modelo,
c_procede,
n_costoact,
n_costoant,
n_costopro,
n_costorep,
n_precio1,
n_precio2,
n_precio3,
c_seriales,
c_compuesto,
c_presenta,
n_peso,
n_volumen,
n_cantibul,
n_pesobul,
n_volbul,
c_fileimagen,
n_impuesto1,
n_impuesto2,
n_impuesto3,
c_cod_arancel,
c_des_arancel,
n_por_arancel,
n_costo_original,
c_observacio,
n_activo,
n_tipopeso,
n_precioO,
f_inicial,
f_final,
h_inicial,
h_final,
add_date,
update_date,
c_codfabricante,
HABLADOR,
C_CODMONEDA,
CANT_DECIMALES,
MONEDA_ANT,
MONEDA_ACT,
MONEDA_PRO,
C_CODIGO_BASE,
C_DESCRI_BASE,
TEXT1,
TEXT2,
TEXT3,
TEXT4,
TEXT5,
TEXT6,
TEXT7,
TEXT8,
DATE1,
NUME1,
N_CANTIDAD_TMP,
C_COD_PLANTILLA,
c_usuarioAdd,
c_usuarioupd,
N_PROD_EXT,
N_PRO_EXT,
NU_TIPO_PRODUCTO,
nu_insumointerno,
nu_precioregulado,
nu_pocentajemerma,
nu_nivelClave,
CU_DESCRIPCION_CORTA,
bs_permiteteclado,
bs_permitecantidad,
nu_stockmin,
nu_stockmax FROM [$(SERVERNAME)].[VAD20].[DBO].[MA_PRODUCTOS]

INSERT INTO [MA_SORTEOS] (TIPO,
f_inicio,
f_final,
condicion,
acumulado,
mensaje,
mensaje1,
mensaje2,
mult_tickets,
BS_ACTIVO,
BS_AFILIADOS,
bs_cumpleanos,
CS_CODIGO_SORTEO,
NU_MONTOMAXIMO,
cs_ORGANIZACION,
cs_LOCALIDAD,
bs_imprimirtickets,
nu_tiposorteo,
bs_acumula,
bs_ImpPtosCompra,
bs_ImpPtosAcumulados,
nu_tipoImpresionTickets,
cs_nombresorteo,
bs_sorteocombinado,
nu_cantidadcombinada,
nu_valorcombinado,
bs_sorteocombinado_montomin )
SELECT TIPO,
f_inicio,
f_final,
condicion,
acumulado,
mensaje,
mensaje1,
mensaje2,
mult_tickets,
BS_ACTIVO,
BS_AFILIADOS,
bs_cumpleanos,
CS_CODIGO_SORTEO,
NU_MONTOMAXIMO,
cs_ORGANIZACION,
cs_LOCALIDAD,
bs_imprimirtickets,
nu_tiposorteo,
bs_acumula,
bs_ImpPtosCompra,
bs_ImpPtosAcumulados,
nu_tipoImpresionTickets,
cs_nombresorteo,
bs_sorteocombinado,
nu_cantidadcombinada,
nu_valorcombinado,
bs_sorteocombinado_montomin FROM [$(SERVERNAME)].[VAD20].[DBO].[MA_SORTEOS]

INSERT INTO [MA_VENTAS_VOLUMEN] (c_codigo,
desde,
hasta,
n_precio1,
n_precio2,
n_precio3,
N_VOLUMEN )
SELECT c_codigo,
desde,
hasta,
n_precio1,
n_precio2,
n_precio3,
N_VOLUMEN FROM [$(SERVERNAME)].[VAD20].[DBO].[MA_VENTAS_VOLUMEN]

IF EXISTS(SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'MA_PROMOCION')
BEGIN
	
	DECLARE @TmpCols1 NVARCHAR(MAX)
	DECLARE @TmpCols2 NVARCHAR(MAX)
	DECLARE @TmpCols3 NVARCHAR(MAX)
	DECLARE @TmpCols4 NVARCHAR(MAX)

	SET @TmpCols1 = (SELECT 'MA_PROMOCION.' + COLUMN_NAME + ',' AS 'data()'
	FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'MA_PROMOCION'
	AND COLUMN_NAME NOT IN ('ID')
	FOR XML PATH (''))
	SET @TmpCols1 = SUBSTRING(@TmpCols1, 1, LEN(@TmpCols1) - 1)

	SET @TmpCols2 = (SELECT 'MA_PROMOCION_SUCURSAL.' + COLUMN_NAME + ',' AS 'data()'
	FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'MA_PROMOCION_SUCURSAL'
	AND COLUMN_NAME NOT IN ('ID')
	FOR XML PATH (''))
	SET @TmpCols2 = SUBSTRING(@TmpCols2, 1, LEN(@TmpCols2) - 1)

	SET @TmpCols3 = (SELECT 'TR_PROMOCION_CONDICION.' + COLUMN_NAME + ',' AS 'data()'
	FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'TR_PROMOCION_CONDICION'
	AND COLUMN_NAME NOT IN ('ID')
	FOR XML PATH (''))
	SET @TmpCols3 = SUBSTRING(@TmpCols3, 1, LEN(@TmpCols3) - 1)

	SET @TmpCols4 = (SELECT 'TR_PROMOCION_VALORES.' + COLUMN_NAME + ',' AS 'data()'
	FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'TR_PROMOCION_VALORES'
	AND COLUMN_NAME NOT IN ('ID')
	FOR XML PATH (''))
	SET @TmpCols4 = SUBSTRING(@TmpCols4, 1, LEN(@TmpCols4) - 1)

	SET @TmpQuery = 'TRUNCATE TABLE [MA_PROMOCION]'
	EXEC (@TmpQuery)
	SET @TmpQuery = 'TRUNCATE TABLE [MA_PROMOCION_SUCURSAL]'
	EXEC (@TmpQuery)
	SET @TmpQuery = 'TRUNCATE TABLE [TR_PROMOCION_CONDICION]'
	EXEC (@TmpQuery)
	SET @TmpQuery = 'TRUNCATE TABLE [TR_PROMOCION_VALORES]'
	EXEC (@TmpQuery)

	SET @TmpQuery = 'INSERT INTO [MA_PROMOCION] (' + REPLACE(@TmpCols1 COLLATE Modern_Spanish_CI_AS, 'MA_PROMOCION.', '') + ')' + CHAR(13) + 
	'SELECT ' + REPLACE(@TmpCols1 COLLATE Modern_Spanish_CI_AS, 'MA_PROMOCION.', '') + ' FROM [$(SERVERNAME)].[VAD10].[DBO].[MA_PROMOCION]' + CHAR(13) + 
	'WHERE 1 = 1 AND Cod_Promocion IN (SELECT Cod_Promocion FROM [$(SERVERNAME)].[VAD10].[dbo].[MA_PROMOCION_SUCURSAL] WHERE Cod_Localidad IN (SELECT c_CodLocalidad FROM [$(SERVERNAME)].[VAD20].[DBO].[MA_CAJA] WHERE C_Codigo = ''$(POSNUMBER)''))'
	EXEC (@TmpQuery)

	SET @TmpQuery = 'INSERT INTO [MA_PROMOCION_SUCURSAL] (' + REPLACE(@TmpCols2 COLLATE Modern_Spanish_CI_AS, 'MA_PROMOCION_SUCURSAL.', '') + ')' + CHAR(13) + 
	'SELECT ' + REPLACE(@TmpCols2 COLLATE Modern_Spanish_CI_AS, 'MA_PROMOCION_SUCURSAL.', '') + ' FROM [$(SERVERNAME)].[VAD10].[DBO].[MA_PROMOCION_SUCURSAL]' + CHAR(13) + 
	'WHERE 1 = 1 AND Cod_Promocion IN (SELECT Cod_Promocion FROM [$(SERVERNAME)].[VAD10].[dbo].[MA_PROMOCION_SUCURSAL] WHERE Cod_Localidad IN (SELECT c_CodLocalidad FROM [$(SERVERNAME)].[VAD20].[DBO].[MA_CAJA] WHERE C_Codigo = ''$(POSNUMBER)''))'
	EXEC (@TmpQuery)

	SET @TmpQuery = 'INSERT INTO [TR_PROMOCION_CONDICION] (' + REPLACE(@TmpCols3 COLLATE Modern_Spanish_CI_AS, 'TR_PROMOCION_CONDICION.', '') + ')' + CHAR(13) + 
	'SELECT ' + REPLACE(@TmpCols3 COLLATE Modern_Spanish_CI_AS, 'TR_PROMOCION_CONDICION.', '') + ' FROM [$(SERVERNAME)].[VAD10].[DBO].[TR_PROMOCION_CONDICION]' + CHAR(13) + 
	'WHERE 1 = 1 AND Cod_Promocion IN (SELECT Cod_Promocion FROM [$(SERVERNAME)].[VAD10].[dbo].[MA_PROMOCION_SUCURSAL] WHERE Cod_Localidad IN (SELECT c_CodLocalidad FROM [$(SERVERNAME)].[VAD20].[DBO].[MA_CAJA] WHERE C_Codigo = ''$(POSNUMBER)''))'
	EXEC (@TmpQuery)

	SET @TmpQuery = 'INSERT INTO [TR_PROMOCION_VALORES] (' + REPLACE(@TmpCols4 COLLATE Modern_Spanish_CI_AS, 'TR_PROMOCION_VALORES.', '') + ')' + CHAR(13) + 
	'SELECT ' + REPLACE(@TmpCols4 COLLATE Modern_Spanish_CI_AS, 'TR_PROMOCION_VALORES.', '') + ' FROM [$(SERVERNAME)].[VAD10].[DBO].[TR_PROMOCION_VALORES]' + CHAR(13) + 
	'WHERE 1 = 1 AND Cod_Promocion IN (SELECT Cod_Promocion FROM [$(SERVERNAME)].[VAD10].[dbo].[MA_PROMOCION_SUCURSAL] WHERE Cod_Localidad IN (SELECT c_CodLocalidad FROM [$(SERVERNAME)].[VAD20].[DBO].[MA_CAJA] WHERE C_Codigo = ''$(POSNUMBER)''))'
	EXEC (@TmpQuery)

END

-- ULTIMO PASO RESTABLECER TR_CAJA PARA EVITAR PROBLEMAS EN CIERRES.

IF NOT EXISTS(SELECT * FROM [TR_CAJA] WHERE cs_Caja = '$(POSNUMBER)' AND c_Estado NOT IN ('C'))
BEGIN
	
	PRINT 'NO HAY DATOS EN TR_CAJA LOCAL.' + CHAR(13) + 'INTENTAR REESTABLECER DESDE EL SERVIDOR.'
	
	SET @TmpQuery = '
	INSERT INTO [TR_CAJA] (cs_CAJA, C_Codigo, C_Cajero, C_Desc_Cajero, N_Cortes, N_Vendido, N_Compras, c_estado, h_inicio, h_final, n_suspendido, Turno, d_fecha_inicio, d_fecha_cierre, B_INVENTARIO, N_MONTOREINTEGRO_TOT, N_REINTEGRO_TOT, cs_ORGANIZACION, cs_LOCALIDAD, ns_TURNO, cs_ESTADO, ds_FECHA_INICIO, ds_FECHA_CIERRE, cs_CAJERO, cs_NOMBRE_CAJERO, ns_NUMERO_OPERACIONES, ns_MONTO_VENDIDO, ns_NUMERO_DEVOLUCIONES, ns_MONTO_DEVUELTO, nu_REINTEGROS_TOTALES, nu_MONTOREINTEGRO_TOTAL, nu_monto_retiros, ns_CORTES, ns_SUSPENDIDO, bs_INVENTARIO'

	IF EXISTS(SELECT * FROM [$(SERVERNAME)].[VAD20].[INFORMATION_SCHEMA].[COLUMNS] WHERE TABLE_NAME = 'TR_CAJA' AND COLUMN_NAME = 'nu_MontoFondo')
		SET @ColumnExists = 1
	ELSE
		SET @ColumnExists = 0

	IF (@ColumnExists = 1)
	BEGIN
		SET @TmpQuery = @TmpQuery + ', nu_MontoFondo'
	END 	
	
	SET @TmpQuery = @TmpQuery +	')'

	SET @TmpQuery = @TmpQuery  + CHAR(13) + '
	SELECT cs_CAJA, C_Codigo, C_Cajero, C_Desc_Cajero, N_Cortes, N_Vendido, N_Compras, c_estado, h_inicio, h_final, n_suspendido, Turno, d_fecha_inicio, d_fecha_cierre, B_INVENTARIO, N_MONTOREINTEGRO_TOT, N_REINTEGRO_TOT, cs_ORGANIZACION, cs_LOCALIDAD, ns_TURNO, cs_ESTADO, ds_FECHA_INICIO, ds_FECHA_CIERRE, cs_CAJERO, cs_NOMBRE_CAJERO, ns_NUMERO_OPERACIONES, ns_MONTO_VENDIDO, ns_NUMERO_DEVOLUCIONES, ns_MONTO_DEVUELTO, nu_REINTEGROS_TOTALES, nu_MONTOREINTEGRO_TOTAL, nu_monto_retiros, ns_CORTES, ns_SUSPENDIDO, bs_INVENTARIO'

	IF (@ColumnExists = 1)
	BEGIN
		SET @TmpQuery = @TmpQuery + ', nu_MontoFondo'
	END

	SET @TmpQuery = @TmpQuery + CHAR(13) + '
	FROM [$(SERVERNAME)].[VAD20].[DBO].[TR_CAJA] WHERE cs_Caja = ''$(POSNUMBER)'' AND cs_EStado NOT IN (''C'')'

	EXEC (@TmpQuery)

	PRINT CHAR(13) + 'SI EL COMANDO ANTERIOR DICE ROWS AFFECTED (1) O MAS ESTA BIEN.' + CHAR(13) + 'INDICA QUE SE ENCONTRO LA INFORMACION DE VENTAS DESDE EL SERVIDOR.' + CHAR(13) + 'EN CASO CONTRARIO, NO HABIA NINGUN REGISTRO DE VENTAS ASI QUE PROBABLEMENTE LA CAJA ESTUVIERA CERRADA.' + CHAR(13) + CHAR(13) + 'DE IGUAL FORMA YA NO DEBERIA HABER NINGUN PROBLEMA.' 

END
ELSE
BEGIN
	PRINT 'HAY DATOS EN TR_CAJA LOCAL.' + CHAR(13) + 'PUDIERA CONTENER ALGUNA INFORMACIÓN DE VENTAS FUERA DE LINEA.' + CHAR(13) + 'EN ESTE CASO ES MEJOR OMITIR LA ACTUALIZACIÓN Y MANTENER EL REGISTRO ACTUAL.' + CHAR(13) + 'NO DEBERIA OCURRIR NINGUN PROBLEMA EN EL CIERRE BAJO ESTAS CIRCUNSTANCIAS.'
END

COMMIT TRANSACTION

-- PROCESO COMPLETADO
PRINT 'Actualizacion Completada'

-- INICIO PROCESO DE BORRADO ACTUALIZACIONES PENDIENTES EN LAS TR_PEND

PRINT 'Inicio de borrado de datos pendientes de la Caja.'

DELETE FROM [$(SERVERNAME)].[VAD20].[DBO].[TR_PEND_BANCOS] WHERE n_Caja = '$(POSNUMBER)'
DELETE FROM [$(SERVERNAME)].[VAD20].[DBO].[TR_PEND_CLIENTES] WHERE n_Caja = '$(POSNUMBER)'
DELETE FROM [$(SERVERNAME)].[VAD20].[DBO].[TR_PEND_DENOMINACION] WHERE n_Caja = '$(POSNUMBER)'
DELETE FROM [$(SERVERNAME)].[VAD20].[DBO].[TR_PEND_MONEDAS] WHERE n_Caja = '$(POSNUMBER)'
DELETE FROM [$(SERVERNAME)].[VAD20].[DBO].[TR_PEND_USUARIOS] WHERE n_Caja = '$(POSNUMBER)'

DELETE FROM [$(SERVERNAME)].[VAD20].[DBO].[TR_PEND_CODIGOS] WHERE n_Caja = '$(POSNUMBER)'
DELETE FROM [$(SERVERNAME)].[VAD20].[DBO].[TR_PEND_PRODUCTOS] WHERE n_Caja = '$(POSNUMBER)'
DELETE FROM [$(SERVERNAME)].[VAD20].[DBO].[TR_PEND_ETIQUETAS] WHERE n_Caja = '$(POSNUMBER)'
DELETE FROM [$(SERVERNAME)].[VAD20].[DBO].[TR_PEND_VENTAS_VOLUMEN] WHERE n_Caja = '$(POSNUMBER)'

IF EXISTS(SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'TR_PEND_PROMOCION')
BEGIN
	SET @TmpQuery = 'DELETE FROM [$(SERVERNAME)].[VAD20].[DBO].[TR_PEND_PROMOCION] WHERE n_Caja = ''$(POSNUMBER)'''
	EXEC (@TmpQuery)
	SET @TmpQuery = 'DELETE FROM [$(SERVERNAME)].[VAD20].[DBO].[TR_PEND_PROMOCION_SUCURSAL] WHERE n_Caja = ''$(POSNUMBER)'''
	EXEC (@TmpQuery)
	SET @TmpQuery = 'DELETE FROM [$(SERVERNAME)].[VAD20].[DBO].[TR_PEND_PROMOCION_CONDICION] WHERE n_Caja = ''$(POSNUMBER)'''
	EXEC (@TmpQuery)
	SET @TmpQuery = 'DELETE FROM [$(SERVERNAME)].[VAD20].[DBO].[TR_PEND_PROMOCION_VALORES] WHERE n_Caja = ''$(POSNUMBER)'''
	EXEC (@TmpQuery)
END

-- PROCESO CULMINADO

PRINT 'Proceso Culminado.'

END
ELSE
BEGIN
	SET @ErrorVar = @@ERROR
	PRINT 'ERROR ? => ' + CAST(@ErrorVar AS NVARCHAR(MAX))
	PRINT 'No hay conexión al servidor.'
END