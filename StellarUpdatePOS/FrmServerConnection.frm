VERSION 5.00
Begin VB.Form FrmServerConnection 
   BorderStyle     =   0  'None
   Caption         =   "UpdatePOS Utility"
   ClientHeight    =   6465
   ClientLeft      =   0
   ClientTop       =   0
   ClientWidth     =   10815
   Icon            =   "FrmServerConnection.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   6465
   ScaleWidth      =   10815
   StartUpPosition =   2  'CenterScreen
   Begin VB.CheckBox chkVerHabladoresPendientes 
      Appearance      =   0  'Flat
      Caption         =   "Ver Habladores Pendientes"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00585A58&
      Height          =   240
      Left            =   5280
      TabIndex        =   20
      Top             =   6000
      Visible         =   0   'False
      Width           =   4110
   End
   Begin VB.CheckBox chkVerCantPendientes 
      Appearance      =   0  'Flat
      Caption         =   "Ver Cantidad de Pendientes"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00585A58&
      Height          =   240
      Left            =   1020
      TabIndex        =   19
      Top             =   6000
      Visible         =   0   'False
      Width           =   3810
   End
   Begin VB.CheckBox chkTeclado 
      Appearance      =   0  'Flat
      Caption         =   "Teclado"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00585A58&
      Height          =   240
      Left            =   1260
      TabIndex        =   18
      Top             =   1980
      Width           =   2430
   End
   Begin VB.CheckBox chkAlinearVAD20 
      Appearance      =   0  'Flat
      Caption         =   "Alinear BD POS [VAD20]"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00585A58&
      Height          =   240
      Left            =   4920
      TabIndex        =   17
      Top             =   1980
      Width           =   4110
   End
   Begin VB.CheckBox chkActualizarPendientes 
      Appearance      =   0  'Flat
      Caption         =   "Actualizar Datos desde TR_PEND [VAD10]"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00585A58&
      Height          =   240
      Left            =   4920
      TabIndex        =   16
      Top             =   2280
      Width           =   4110
   End
   Begin VB.TextBox txtCnTimeout 
      Appearance      =   0  'Flat
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   0  'None
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00585A58&
      Height          =   435
      Left            =   4980
      MaxLength       =   50
      TabIndex        =   14
      Text            =   "20"
      Top             =   1320
      Width           =   4080
   End
   Begin VB.CommandButton cmd_salir 
      Caption         =   "Salir"
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   855
      Left            =   6540
      Picture         =   "FrmServerConnection.frx":628A
      Style           =   1  'Graphical
      TabIndex        =   13
      Top             =   4800
      Width           =   1125
   End
   Begin VB.CommandButton aceptar 
      Caption         =   "Conectar"
      CausesValidation=   0   'False
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   855
      Left            =   2640
      Picture         =   "FrmServerConnection.frx":800C
      Style           =   1  'Graphical
      TabIndex        =   12
      Top             =   4800
      Width           =   1125
   End
   Begin VB.CheckBox chkAuth 
      Appearance      =   0  'Flat
      Caption         =   "Requiere Autenticación"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00585A58&
      Height          =   240
      Left            =   1260
      TabIndex        =   11
      Top             =   2280
      Width           =   2430
   End
   Begin VB.Frame Frame_Datos_Auth 
      BackColor       =   &H00E7E8E8&
      BorderStyle     =   0  'None
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00585A58&
      Height          =   1875
      Left            =   1260
      TabIndex        =   5
      Top             =   2700
      Visible         =   0   'False
      Width           =   8115
      Begin VB.TextBox txtClave 
         Appearance      =   0  'Flat
         BackColor       =   &H00FFFFFF&
         BorderStyle     =   0  'None
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   435
         IMEMode         =   3  'DISABLE
         Left            =   3840
         MaxLength       =   50
         PasswordChar    =   "*"
         TabIndex        =   7
         Top             =   1125
         Width           =   4080
      End
      Begin VB.TextBox txtUsuario 
         Appearance      =   0  'Flat
         BackColor       =   &H00FFFFFF&
         BorderStyle     =   0  'None
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   435
         Left            =   3840
         MaxLength       =   50
         TabIndex        =   6
         Text            =   "SA"
         Top             =   400
         Width           =   4080
      End
      Begin VB.Label lblUsuario 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Usuario (Login): "
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   285
         Left            =   840
         TabIndex        =   10
         Top             =   480
         Width           =   1815
      End
      Begin VB.Label lblPassword 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Clave:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   285
         Left            =   840
         TabIndex        =   9
         Top             =   1140
         Width           =   1560
      End
      Begin VB.Label lblCredenciales 
         BackColor       =   &H00E7E8E8&
         Caption         =   "Credenciales"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   11.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00AE5B00&
         Height          =   375
         Left            =   240
         TabIndex        =   8
         Top             =   0
         Width           =   2655
      End
   End
   Begin VB.TextBox txtServer 
      Appearance      =   0  'Flat
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   0  'None
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00585A58&
      Height          =   435
      Left            =   4980
      MaxLength       =   50
      TabIndex        =   3
      Top             =   780
      Width           =   4080
   End
   Begin VB.Frame Frame5 
      Appearance      =   0  'Flat
      BackColor       =   &H00AE5B00&
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   480
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   10920
      Begin VB.Label lbl_website 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "www.mistellar.com"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FAFAFA&
         Height          =   255
         Left            =   7995
         TabIndex        =   2
         Top             =   105
         Width           =   1815
      End
      Begin VB.Label lbl_Organizacion 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "Stellar UpdatePOS Utility"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FAFAFA&
         Height          =   255
         Left            =   240
         TabIndex        =   1
         Top             =   105
         Width           =   5295
      End
      Begin VB.Image Exit 
         Appearance      =   0  'Flat
         Height          =   480
         Left            =   10320
         Picture         =   "FrmServerConnection.frx":9D8E
         Top             =   0
         Width           =   480
      End
   End
   Begin VB.Label lblTimeout 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "Timeout Conexion (Seg):"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00585A58&
      Height          =   285
      Left            =   1260
      TabIndex        =   15
      Top             =   1395
      Width           =   2685
   End
   Begin VB.Label lblServer 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "IP / Instancia del Servidor:"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00585A58&
      Height          =   285
      Left            =   1260
      TabIndex        =   4
      Top             =   855
      Width           =   2850
   End
End
Attribute VB_Name = "FrmServerConnection"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub aceptar_Click()
    
    On Error GoTo Error
    
    Me.Enabled = False
    
    RespFin = ShellEx(0, "open", App.Path & "\Messages\SendMessage_RequiresParams.exe", _
    "[StellarInstallerExternalProgressID SAFE APP CLOSE]", "", 1)
    Sleep 100
    
    Resp1 = ShellEx(0, "open", App.Path & "\Messages\Progress.exe", _
    "|p|Title=UpdatePOS Utility|p|Subtitle=" & RemoverAcentos(StellarMensaje(609)), "", 1)  ' Comprobando Conexion
    
    Sleep 300
    
    Resp2 = ShellEx(0, "open", App.Path & "\Messages\SendMessage_RequiresParams.exe", _
    "..." & StellarMensaje(610) & "...", "", 1) 'Por favor espere
    
    Sleep 200
    
    Set pCn = New Connection
    
    CadenaConexionVAD10_ValidarTimeout = "Driver={SQL Server};Server=" & txtServer & ";Database=VAD10;" & _
    "Persist Security Info=True;Uid=" & txtUsuario & ";Pwd=" & txtClave & ";"
    
    Dim TmpTime, HoraInicioValidarConexion, HoraFinValidarConexion
    
    HoraInicioValidarConexion = Now
    
    pCn.ConnectionString = CadenaConexionVAD10_ValidarTimeout
    
    pCn.Open , , Options:=adAsyncConnect
    
    TmpTime = Timer()
    
    Do Until (Timer() - TmpTime) > (Val(txtCnTimeout)) _
    Or pCn.State = adStateOpen
        DoEvents
    Loop
    
    If Not (pCn.State = adStateOpen) Then
        Err.Raise 999, , "Timeout"
    End If
    
Error:
    
    mErrorNumber = Err.Number
    mErrorDesc = Err.Description
    mErrorSource = Err.Source
    
    Me.Enabled = True
    
    If PuedeObtenerFoco(Me) Then Me.SetFocus
    SetWindowPos Me.hWnd, 0, 0, 0, 0, 0, _
    (SWP_SHOWWINDOW & SWP_NOMOVE & SWP_NOREPOSITION & SWP_NOREDRAW & SWP_NOSIZE)
    
    If mErrorNumber <> 0 Then
        MsgBox mErrorDesc & " " & "(" & mErrorNumber & ")."
    ElseIf pCn.State = adStateOpen Then
        
        MsgBox StellarMensaje(627) '"Conexion Establecida"
        
        KillSecure App.Path & "\ERRORLEVEL.TXT"
        
        RespFin = ShellEx(0, "open", App.Path & "\Messages\SendMessage_RequiresParams.exe", _
        "[StellarInstallerExternalProgressID SAFE APP CLOSE]", "", 1)
        
        Sleep 100
        
        Resp1 = ShellEx(0, "open", App.Path & "\Messages\Progress.exe", _
        "|p|Title=UpdatePOS Utility|p|Subtitle=" & StellarMensaje(626) & "", "", 1) 'Actualizando Servidor
        Resp3 = ShellEx(0, "open", App.Path & "\Messages\SendMessage_RequiresParams.exe", _
        "..." & StellarMensaje(611) & "...", "", 1) 'Sincronizando BD POS [VAD20] + Limpiar Temporales
        
        RespScript = ShellAndWait(App.Path & "\Server_Update.bat" & _
        " " & txtServer & " " & txtUsuario & " """ & txtClave & """ " & _
        IIf(chkActualizarPendientes = vbChecked, 1, 0) & " " & IIf(chkAlinearVAD20 = vbChecked, 1, 0), vbHide) ', vbNormalFocus)
        Resp4 = ShellEx(0, "open", App.Path & "\Messages\SendMessage_RequiresParams.exe", _
        "..." & StellarMensaje(612) & "...", "", 1) 'verificando estatus
        
        Sleep 200
        
        ErrorLevel = Val(LoadTextFile(App.Path & "\ERRORLEVEL.TXT"))
        
        Success = (ErrorLevel = 0)
        
        KillSecure App.Path & "\ERRORLEVEL.TXT"
        
        'RespScript = ShellAndWait(App.Path & "\POS_Update.bat " & txtServer & " " & txtUsuario & " """ & txtClave & """ " & txtServer & " " & "002", vbHide)
        'Sleep 200
        'Success = (Val(LoadTextFile(App.Path & "\ERRORLEVEL.TXT")) = 0)
        
        If PuedeObtenerFoco(Me) Then Me.SetFocus
        SetWindowPos Me.hWnd, 0, 0, 0, 0, 0, _
        (SWP_SHOWWINDOW & SWP_NOMOVE & SWP_NOREPOSITION & SWP_NOREDRAW & SWP_NOSIZE)
        
        SaveTextFile txtServer, App.Path & "\LastServer.dat"
        SaveTextFile txtuser, App.Path & "\LastUser.dat"
        SaveTextFile txtCnTimeout, App.Path & "\LastCnTO.dat"
        SaveTextFile chkAuth, App.Path & "\LastAuth.dat"
        SaveTextFile chkAlinearVAD20, App.Path & "\LastActBDPOS.dat"
        SaveTextFile chkActualizarPendientes, App.Path & "\LastActPend.dat"
        SaveTextFile chkTeclado, App.Path & "\LastKeyboard.dat"
        
        If Success Then
            
            RespFin = ShellEx(0, "open", App.Path & "\Messages\SendMessage_RequiresParams.exe", _
            "[StellarInstallerExternalProgressID SAFE APP CLOSE]", "", 1)
            'MsgBox "Actualización del Servidor Completada." & vbNewLine & "Se han eliminado los pendientes por caja y cola de habladores." & vbNewLine & "A partir de este momento debe alinear todas las cajas." & vbNewLine & "Presione Aceptar para continuar."
            'MsgBox _
            IIf(chkActualizarPendientes = vbChecked, "Actualización del Servidor Completada." & vbNewLine, Empty) & _
            IIf(chkAlinearVAD20 = vbChecked, "Se han eliminado los pendientes por caja y cola de habladores." & vbNewLine, Empty) & _
            IIf(chkAlinearVAD20 = vbChecked, "A partir de este momento debe alinear todas las cajas." & vbNewLine, Empty) & _
            "Presione Aceptar para continuar." _

            MsgBox _
            IIf(chkActualizarPendientes = vbChecked, "" & StellarMensaje(613) & "" & vbNewLine, Empty) & _
            IIf(chkAlinearVAD20 = vbChecked, "" & StellarMensaje(614) & "" & vbNewLine, Empty) & _
            IIf(chkAlinearVAD20 = vbChecked, "" & StellarMensaje(615) & "" & vbNewLine, Empty) & _
            "" & StellarMensaje(16030) & ""
            
            Ficha_Entrada.Show vbModal
            
        Else
            MsgBox "" & StellarMensaje(616) & ""
        End If
        
    End If
    
    RespFin = ShellEx(0, "open", App.Path & "\Messages\SendMessage_RequiresParams.exe", _
    "[StellarInstallerExternalProgressID SAFE APP CLOSE]", "", 1)
    
End Sub

Private Sub chkActualizarPendientes_Click()
    chkVerCantPendientes.Visible = (chkActualizarPendientes = vbChecked)
End Sub

Private Sub chkAlinearVAD20_Click()
    chkVerHabladoresPendientes.Visible = (chkAlinearVAD20 = vbChecked)
End Sub

Private Sub chkAuth_Click()
    Frame_Datos_Auth.Visible = chkAuth.Value = vbChecked
End Sub

Private Sub chkTeclado_Click()
    ModoTouch = (chkTeclado = vbChecked)
End Sub

Private Sub chkVerCantPendientes_Click()
    If chkVerCantPendientes = vbChecked Then
        chkVerCantPendientes = vbUnchecked
        On Error Resume Next
        Dim Resumen As String, mRsCant As ADODB.Recordset
        Resumen = "" & StellarMensaje(630) & ":" & vbNewLine & vbNewLine
        Set pCn = New ADODB.Connection
        CadenaConexionVAD10_ValidarTimeout = "Driver={SQL Server};Server=" & txtServer & ";Database=VAD10;" & _
        "Persist Security Info=True;Uid=" & txtUsuario & ";Pwd=" & txtClave & ";"
        pCn.ConnectionString = CadenaConexionVAD10_ValidarTimeout
        pCn.Open
        Set mRsCant = pCn.Execute("SELECT ROUND(isNULL(COUNT(*), 0), 8, 0) AS Cant FROM VAD10.DBO.TR_PENDIENTE_PROD")
        Resumen = Resumen & "" & StellarMensaje(631) & ": " & mRsCant!Cant & vbNewLine & vbNewLine
        Set mRsCant = pCn.Execute("SELECT ROUND(isNULL(COUNT(*), 0), 8, 0) AS Cant FROM VAD10.DBO.TR_PENDIENTE_CODIGO")
        Resumen = Resumen & "" & StellarMensaje(632) & ": " & mRsCant!Cant & vbNewLine & vbNewLine
        Set mRsCant = pCn.Execute("SELECT ROUND(isNULL(COUNT(*), 0), 8, 0) AS Cant FROM VAD10.DBO.TR_PEND_DEPARTAMENTOS")
        Resumen = Resumen & "" & StellarMensaje(633) & ": " & mRsCant!Cant & vbNewLine & vbNewLine
        Set mRsCant = pCn.Execute("SELECT ROUND(isNULL(COUNT(*), 0), 8, 0) AS Cant FROM VAD10.DBO.TR_PEND_GRUPOS")
        Resumen = Resumen & "" & StellarMensaje(634) & ": " & mRsCant!Cant & vbNewLine & vbNewLine
        Set mRsCant = pCn.Execute("SELECT ROUND(isNULL(COUNT(*), 0), 8, 0) AS Cant FROM VAD10.DBO.TR_PEND_SUBGRUPOS")
        Resumen = Resumen & "" & StellarMensaje(635) & ": " & mRsCant!Cant & vbNewLine & vbNewLine
        MsgBox Resumen
    End If
End Sub

Private Sub chkVerHabladoresPendientes_Click()
    If chkVerHabladoresPendientes = vbChecked Then
        chkVerHabladoresPendientes = vbUnchecked
        On Error Resume Next
        Dim Resumen As String, mRsCant As ADODB.Recordset
        Resumen = "" & StellarMensaje(636) & ":" & vbNewLine & vbNewLine
        Set pCn = New ADODB.Connection
        CadenaConexionVAD10_ValidarTimeout = "Driver={SQL Server};Server=" & txtServer & ";Database=VAD10;" & _
        "Persist Security Info=True;Uid=" & txtUsuario & ";Pwd=" & txtClave & ";"
        pCn.ConnectionString = CadenaConexionVAD10_ValidarTimeout
        pCn.Open
        Set mRsCant = pCn.Execute("SELECT ROUND(isNULL(COUNT(*), 0), 8, 0) AS Cant FROM VAD20.DBO.TR_PENDIENTE_PROD")
        Resumen = Resumen & "" & StellarMensaje(631) & ": " & mRsCant!Cant & vbNewLine & vbNewLine
        Set mRsCant = pCn.Execute("SELECT ROUND(isNULL(COUNT(*), 0), 8, 0) AS Cant FROM VAD20.DBO.TR_PENDIENTE_CODIGO")
        Resumen = Resumen & "" & StellarMensaje(632) & ": " & mRsCant!Cant & vbNewLine & vbNewLine
        MsgBox Resumen
    End If
End Sub

Private Sub cmd_salir_Click()
    End
End Sub

Private Sub Exit_Click()
    cmd_salir_Click
End Sub

Private Sub Form_Load()
    
    KillSecure App.Path & "\ERRORLEVEL.TXT"
    
    RespCheck = ShellAndWait(App.Path & "\CheckDependencies.bat" & " " & "1", vbHide)
    
    ErrorLevel = Val(LoadTextFile(App.Path & "\ERRORLEVEL.TXT"))
    
    If ErrorLevel < 0 Then
        RespCheck = ShellAndWait(App.Path & "\CheckDependencies.bat" & " " & "0", vbHide)
        ErrorLevel = Val(LoadTextFile(App.Path & "\ERRORLEVEL.TXT"))
    End If
    
    Success = (ErrorLevel = 0)
    
    KillSecure App.Path & "\ERRORLEVEL.TXT"
    
    If Not Success Then
        'MsgBox "Dependencia faltante, instale Microsoft SQLCMD como Administrador."
        MsgBox StellarMensaje(600)
        End
    End If
    
    txtServer = LoadTextFile(App.Path & "\LastServer.dat")
    txtUsuario = LoadTextFile(App.Path & "\LastUser.dat")
    If Len(Trim(txtUsuario)) <= 0 Then txtUsuario = "SA"
    txtCnTimeout = Val(LoadTextFile(App.Path & "\LastCnTO.dat"))
    If Val(txtCnTimeout) < 0 Then txtCnTimeout = 0
    If Val(txtCnTimeout) = 0 Then txtCnTimeout = 20
    chkAuth = IIf(Val(LoadTextFile(App.Path & "\LastAuth.dat")) = vbChecked, vbChecked, vbUnchecked)
    mVAD20 = LoadTextFile(App.Path & "\LastActBDPOS.dat")
    If Trim(mVAD20) <> Empty Then
        chkAlinearVAD20 = IIf(Val(mVAD20) = vbChecked, vbChecked, vbUnchecked)
    Else
        chkAlinearVAD20 = vbChecked
    End If
    chkActualizarPendientes = IIf(Val(LoadTextFile(App.Path & "\LastActPend.dat")) = vbChecked, vbChecked, vbUnchecked)
    chkTeclado = IIf(Val(LoadTextFile(App.Path & "\LastKeyboard.dat")) = vbChecked, vbChecked, vbUnchecked)
    
    lblServer = StellarMensaje(601) & ":"
    lblTimeout = StellarMensaje(602) & ":"
    chkAuth.Caption = StellarMensaje(8018)
    chkAlinearVAD20.Caption = StellarMensaje(603)
    chkActualizarPendientes.Caption = StellarMensaje(604)
    chkVerCantPendientes.Caption = StellarMensaje(628)
    chkVerHabladoresPendientes.Caption = StellarMensaje(629)
    chkTeclado.Caption = StellarMensaje(500)
    lblCredenciales = StellarMensaje(605)
    lblUsuario = StellarMensaje(606) & ":"
    lblPassword = StellarMensaje(607) & ":"
    aceptar.Caption = StellarMensaje(608)
    cmd_salir.Caption = StellarMensaje(16249)
    
End Sub

Private Sub txtClave_Click()
    If ModoTouch Then
        TecladoPOS CampoT
    End If
End Sub

Private Sub txtClave_GotFocus()
    Set CampoT = txtClave
End Sub

Private Sub txtCnTimeout_Click()
    If ModoTouch Then
        TecladoPOS CampoT
    End If
End Sub

Private Sub txtCnTimeout_GotFocus()
    Set CampoT = txtCnTimeout
End Sub

Private Sub txtServer_Click()
    If ModoTouch Then
        TecladoPOS CampoT
    End If
End Sub

Private Sub txtServer_GotFocus()
    Set CampoT = txtServer
End Sub

Private Sub txtServer_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyReturn Then
        aceptar_Click
    End If
End Sub

Private Sub txtUsuario_Click()
    If ModoTouch Then
        TecladoPOS CampoT
    End If
End Sub

Private Sub txtUsuario_GotFocus()
    Set CampoT = txtUsuario
End Sub
