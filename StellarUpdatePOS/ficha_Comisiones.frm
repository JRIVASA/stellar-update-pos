VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.1#0"; "MSCOMCTL.ocx"
Object = "{50BF2256-701F-46F2-8ADB-2202CE6922BC}#1.0#0"; "KlexGrid.ocx"
Begin VB.Form Ficha_Entrada 
   Appearance      =   0  'Flat
   BorderStyle     =   1  'Fixed Single
   ClientHeight    =   8490
   ClientLeft      =   15
   ClientTop       =   15
   ClientWidth     =   10905
   ControlBox      =   0   'False
   Icon            =   "ficha_Comisiones.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   8490
   ScaleWidth      =   10905
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.TextBox txt 
      Appearance      =   0  'Flat
      BorderStyle     =   0  'None
      Height          =   285
      Left            =   2940
      TabIndex        =   12
      Text            =   "Text1"
      Top             =   1140
      Visible         =   0   'False
      Width           =   435
   End
   Begin VB.CheckBox chkUpdateRemaining 
      Appearance      =   0  'Flat
      Caption         =   "Actualizar todos los POS Faltantes"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00585A58&
      Height          =   240
      Left            =   240
      TabIndex        =   11
      Top             =   780
      Width           =   6510
   End
   Begin VB.CheckBox chkLogin 
      Appearance      =   0  'Flat
      Caption         =   "Requiere Asignar Credenciales"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00585A58&
      Height          =   240
      Left            =   7560
      TabIndex        =   10
      Top             =   780
      Width           =   3090
   End
   Begin VB.Frame FrameEdit 
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   495
      Left            =   4080
      TabIndex        =   8
      Top             =   0
      Visible         =   0   'False
      Width           =   615
      Begin VB.Image CmdEdit 
         Height          =   480
         Left            =   75
         Picture         =   "ficha_Comisiones.frx":628A
         Top             =   0
         Width           =   480
      End
   End
   Begin VB.Frame FrameRetry 
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   495
      Left            =   3360
      TabIndex        =   7
      Top             =   0
      Visible         =   0   'False
      Width           =   615
      Begin VB.Image CmdRetry 
         Height          =   480
         Left            =   75
         Picture         =   "ficha_Comisiones.frx":6F54
         Top             =   0
         Width           =   480
      End
   End
   Begin VB.Frame FrameOK 
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   495
      Left            =   2640
      TabIndex        =   6
      Top             =   0
      Visible         =   0   'False
      Width           =   615
      Begin VB.Image CmdOK 
         Height          =   480
         Left            =   75
         Picture         =   "ficha_Comisiones.frx":8CD6
         Top             =   0
         Width           =   480
      End
   End
   Begin VB.Frame Frame5 
      Appearance      =   0  'Flat
      BackColor       =   &H00AE5B00&
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   480
      Left            =   0
      TabIndex        =   2
      Top             =   0
      Width           =   10920
      Begin VB.Image Exit 
         Appearance      =   0  'Flat
         Height          =   480
         Left            =   10320
         Picture         =   "ficha_Comisiones.frx":99A0
         Top             =   0
         Width           =   480
      End
      Begin VB.Label lbl_Organizacion 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "Stellar UpdatePOS Utility"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FAFAFA&
         Height          =   255
         Left            =   240
         TabIndex        =   4
         Top             =   105
         Width           =   5295
      End
      Begin VB.Label lbl_website 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "www.mistellar.com"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FAFAFA&
         Height          =   255
         Left            =   7995
         TabIndex        =   3
         Top             =   105
         Width           =   1815
      End
   End
   Begin VB.Frame Frame_Detalle 
      BackColor       =   &H00E7E8E8&
      BorderStyle     =   0  'None
      Caption         =   "Formato de Comisiones"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00585A58&
      Height          =   6915
      Left            =   240
      TabIndex        =   1
      Top             =   1320
      Width           =   10455
      Begin VB.VScrollBar ScrollGrid 
         Height          =   6015
         LargeChange     =   10
         Left            =   9540
         TabIndex        =   9
         Top             =   600
         Width           =   675
      End
      Begin Grid.KlexGrid Grid 
         Height          =   6015
         Left            =   240
         TabIndex        =   0
         Top             =   600
         Width           =   9975
         _ExtentX        =   17595
         _ExtentY        =   10610
         EnterKeyBehaviour=   2
         Editable        =   -1  'True
         BackColorAlternate=   16448250
         GridLinesFixed  =   0
         AllowBigSelection=   0   'False
         AllowUserResizing=   1
         BackColor       =   16448250
         BackColorBkg    =   16448250
         BackColorFixed  =   5000268
         BackColorSel    =   15724527
         BorderStyle     =   0
         Cols            =   6
         FixedCols       =   0
         FocusRect       =   0
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   4210752
         ForeColorFixed  =   16777215
         ForeColorSel    =   4210752
         GridColor       =   13421772
         MouseIcon       =   "ficha_Comisiones.frx":B722
         RowHeightMin    =   500
         ScrollTrack     =   -1  'True
      End
      Begin VB.Line Line1 
         BorderColor     =   &H00AE5B00&
         X1              =   840
         X2              =   10150
         Y1              =   240
         Y2              =   240
      End
      Begin VB.Label lblFormato 
         BackColor       =   &H00E7E8E8&
         Caption         =   "POS"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   11.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00AE5B00&
         Height          =   375
         Left            =   240
         TabIndex        =   5
         Top             =   120
         Width           =   2655
      End
   End
   Begin MSComctlLib.ImageList Icono_Apagado 
      Left            =   8880
      Top             =   1320
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   32
      ImageHeight     =   32
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   8
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "ficha_Comisiones.frx":B73E
            Key             =   ""
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "ficha_Comisiones.frx":C418
            Key             =   ""
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "ficha_Comisiones.frx":E1AA
            Key             =   ""
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "ficha_Comisiones.frx":FF3C
            Key             =   ""
         EndProperty
         BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "ficha_Comisiones.frx":11CCE
            Key             =   ""
         EndProperty
         BeginProperty ListImage6 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "ficha_Comisiones.frx":13A60
            Key             =   ""
         EndProperty
         BeginProperty ListImage7 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "ficha_Comisiones.frx":157F2
            Key             =   ""
         EndProperty
         BeginProperty ListImage8 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "ficha_Comisiones.frx":17584
            Key             =   ""
         EndProperty
      EndProperty
   End
End
Attribute VB_Name = "Ficha_Entrada"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private mRowCell As Long, mColCell As Long, Fila As Long
Private Band As Boolean

Private SilentMode As Boolean

Private Enum KCol
    
    ColCodCaja
    ColDescCaja
    ColHiddenTipoPOS
    ColInstancia
    ColOptionalLogin
    ColOptionalPassword
    ColPicEstatus
    ColHiddenAux
    
    ColCount ' Last Item. Not a Col. Just a constant for Column Count
    
End Enum

Private Sub CargarCajas()
    
    CreateFullDirectoryPath App.Path & "\LastPOSInfo\Info.dat"
    
    On Error GoTo Error
    
    Dim mRs As ADODB.Recordset
    Set mRs = New ADODB.Recordset
    
    mRs.Open "SELECT c_Codigo, c_Desc_Caja, " & _
    "CASE WHEN b_Pos_Pedido = 0 AND b_POS_Digital = 0 THEN 0 ELSE 1 END AS TipoPOS " & _
    "FROM VAD20.DBO.MA_CAJA ORDER BY c_Desc_Caja", pCn, adOpenStatic, adLockReadOnly, adCmdText
    
    ScrollGrid.Visible = False
    
    If Not mRs.EOF Then
        
        ScrollGrid.Min = 1
        ScrollGrid.Max = mRs.RecordCount
        ScrollGrid.Value = 1
        
        While Not mRs.EOF
            
            With Grid
                
                .Rows = .Rows + 1
                Dim NumRow As Long
                NumRow = .Rows - 1
                .Row = NumRow
                
                .TextMatrix(NumRow, KCol.ColCodCaja) = mRs!c_Codigo
                .TextMatrix(NumRow, KCol.ColDescCaja) = mRs!c_Desc_Caja
                .TextMatrix(NumRow, KCol.ColHiddenTipoPOS) = mRs!TipoPOS
                .TextMatrix(NumRow, KCol.ColInstancia) = LoadTextFile(App.Path & "\LastPOSInfo\IP_POS_" & mRs!c_Codigo & ".dat")
                
                .TextMatrix(NumRow, KCol.ColOptionalLogin) = LoadTextFile(App.Path & "\LastPOSInfo\SL_POS_" & mRs!c_Codigo & ".dat")
                If Trim(.TextMatrix(NumRow, KCol.ColOptionalLogin)) = Empty Then
                    .TextMatrix(NumRow, KCol.ColOptionalLogin) = "SA"
                End If
                
                .TextMatrix(NumRow, KCol.ColOptionalPassword) = LoadTextFile(App.Path & "\LastPOSInfo\SK_POS_" & mRs!c_Codigo & ".dat")
                If Trim(.TextMatrix(NumRow, KCol.ColOptionalPassword)) = Empty Then
                    .TextMatrix(NumRow, KCol.ColOptionalPassword) = Empty
                End If
                
                .Col = KCol.ColPicEstatus
                
                .TextMatrix(NumRow, .Col) = Empty
                Set .CellPicture = CmdEdit.Picture
                .CellPictureAlignment = 4
                .ColData(.Col) = 0
                
                mRs.MoveNext
                
            End With
            
        Wend
        
        If Grid.Rows > 9 Then
            ScrollGrid.Visible = True
            Grid.ScrollBars = 3
        Else
            Grid.ScrollBars = 1
            ScrollGrid.Visible = False
        End If
        
        If PuedeObtenerFoco(Grid) Then Grid.SetFocus
        
        Fila = 0
        Grid_EnterCell
        
    End If
    
    Exit Sub
    
Error:
    
End Sub

Private Sub chkLogin_Click()
    With Grid
    If chkLogin = vbChecked Then
        .ColWidth(KCol.ColCodCaja) = 705
        .ColWidth(KCol.ColDescCaja) = 2385
        .ColWidth(KCol.ColHiddenTipoPOS) = 0
        '.ColIsVisible(KCol.ColHiddenTipoPOS) = 0
        .ColWidth(KCol.ColInstancia) = 2730
        .ColWidth(KCol.ColOptionalLogin) = 1125
        '.ColIsVisible(KCol.ColOptionalLogin) = 0
        .ColWidth(KCol.ColOptionalPassword) = 1440
        '.ColIsVisible(KCol.ColOptionalPassword) = 0
        .ColWidth(KCol.ColHiddenAux) = 0
        '.ColIsVisible(KCol.ColHiddenAux) = 0
        .ColWidth(KCol.ColPicEstatus) = 915
    Else
        .ColWidth(KCol.ColCodCaja) = 1000
        .ColWidth(KCol.ColDescCaja) = 2910
        .ColWidth(KCol.ColHiddenTipoPOS) = 0
        '.ColIsVisible(KCol.ColHiddenTipoPOS) = 0
        .ColWidth(KCol.ColInstancia) = 4470
        .ColWidth(KCol.ColOptionalLogin) = 0
        '.ColIsVisible(KCol.ColOptionalLogin) = 0
        .ColWidth(KCol.ColOptionalPassword) = 0
        '.ColIsVisible(KCol.ColOptionalPassword) = 0
        .ColWidth(KCol.ColHiddenAux) = 0
        '.ColIsVisible(KCol.ColHiddenAux) = 0
        .ColWidth(KCol.ColPicEstatus) = 915
    End If
    End With
End Sub

Private Sub chkUpdateRemaining_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyReturn Then
        chkUpdateRemaining = IIf(chkUpdateRemaining = vbChecked, vbUnchecked, vbChecked)
    End If
End Sub

Private Sub chkUpdateRemaining_MouseUp(Button As Integer, Shift As Integer, X As Single, Y As Single)
    If chkUpdateRemaining = vbChecked Then
                
        SilentMode = True
        
        OriginalText = chkUpdateRemaining.Caption
        
        For i = 1 To Grid.Rows - 1
            
            DoEvents
            
            Grid.Row = i
            
            Grid.Col = KCol.ColPicEstatus
            
            Grid_Click
            
            DoEvents
            
            chkUpdateRemaining.Caption = StellarMensaje(618) '"Presione si desea detener el proceso autom�tico."
            
            DoEvents
            
            If PuedeObtenerFoco(chkUpdateRemaining) Then chkUpdateRemaining.SetFocus
            
            TS = DateAdd("s", 2, Now)
            
            While Now < TS
                DoEvents
                If chkUpdateRemaining <> vbChecked Then
                    Exit For
                End If
            Wend
            
        Next i
        
        MsgBox StellarMensaje(16600) '"Proceso culminado."
        
        chkUpdateRemaining.Caption = OriginalText
        
        SilentMode = False
        
        chkUpdateRemaining = vbUnchecked
    Else
        Debug.Print "Cancelando"
    End If
End Sub

Private Sub Exit_Click()
    
    For i = 1 To Grid.Rows - 1
        
        SaveTextFile Grid.TextMatrix(i, KCol.ColInstancia), App.Path & "\LastPOSInfo\IP_POS_" & Grid.TextMatrix(i, KCol.ColCodCaja) & ".dat"
        SaveTextFile Grid.TextMatrix(i, KCol.ColOptionalLogin), App.Path & "\LastPOSInfo\SL_POS_" & Grid.TextMatrix(i, KCol.ColCodCaja) & ".dat"
        SaveTextFile Grid.TextMatrix(i, KCol.ColOptionalPassword), App.Path & "\LastPOSInfo\SK_POS_" & Grid.TextMatrix(i, KCol.ColCodCaja) & ".dat"
        
    Next i
    
    Unload Me
    
End Sub

Private Sub Form_Load()
    
    chkUpdateRemaining.Caption = StellarMensaje(617)
    chkLogin.Caption = StellarMensaje(8018)
    
    initialize_Klexgrid
    CargarCajas
    chkUpdateRemaining = vbUnchecked
    
End Sub

Private Sub initialize_Klexgrid()
    
    With Grid
        
        .FixedRows = 1
        .Rows = 1
        
        .Cols = KCol.ColCount
        
        .RowHeight(0) = 425
        .RowHeightMin = 600
        
        .ColDisplayFormat(KCol.ColCodCaja) = "&&&&&&&&&&&&&&&&"
        .ColDisplayFormat(KCol.ColDescCaja) = "&&&&&&&&&&&&&&&&"
        .ColDisplayFormat(KCol.ColHiddenTipoPOS) = "&&&&&&&&&&&&&&&&"
        .ColDisplayFormat(KCol.ColInstancia) = "&&&&&&&&&&&&&&&&"
        .ColDisplayFormat(KCol.ColOptionalLogin) = "&&&&&&&&&&&&&&&&"
        .ColDisplayFormat(KCol.ColOptionalPassword) = "&&&&&&&&&&&&&&&&"
        .ColDisplayFormat(KCol.ColPicEstatus) = "&&&&&&&&&&&&&&&&"
        
        .ColWidth(KCol.ColCodCaja) = 1000
        .ColWidth(KCol.ColDescCaja) = 2910
        .ColWidth(KCol.ColHiddenTipoPOS) = 0
        '.ColIsVisible(KCol.ColHiddenTipoPOS) = 0
        .ColWidth(KCol.ColInstancia) = 4470
        .ColWidth(KCol.ColOptionalLogin) = 0
        '.ColIsVisible(KCol.ColOptionalLogin) = 0
        .ColWidth(KCol.ColOptionalPassword) = 0
        '.ColIsVisible(KCol.ColOptionalPassword) = 0
        .ColWidth(KCol.ColHiddenAux) = 0
        '.ColIsVisible(KCol.ColHiddenAux) = 0
        .ColWidth(KCol.ColPicEstatus) = 915
        
        .ColAlignment(KCol.ColCodCaja) = 4
        .ColAlignment(KCol.ColDescCaja) = 1
        .ColAlignment(KCol.ColHiddenTipoPOS) = 7
        .ColAlignment(KCol.ColInstancia) = 1
        .ColAlignment(KCol.ColOptionalLogin) = 1
        .ColAlignment(KCol.ColOptionalPassword) = 1
        .ColAlignment(KCol.ColHiddenAux) = 7
        .ColAlignment(KCol.ColPicEstatus) = 4
        
        .TextMatrix(0, KCol.ColCodCaja) = "POS"
        .TextMatrix(0, KCol.ColDescCaja) = StellarMensaje(16226) '"NAME"
        .TextMatrix(0, KCol.ColHiddenTipoPOS) = "TIPO_POS"
        .TextMatrix(0, KCol.ColInstancia) = "IP"
        .TextMatrix(0, KCol.ColOptionalLogin) = "Login"
        .TextMatrix(0, KCol.ColOptionalPassword) = "Pwd"
        .TextMatrix(0, KCol.ColHiddenAux) = Empty
        .TextMatrix(0, KCol.ColPicEstatus) = Empty
        
        .Col = KCol.ColHiddenAux
        .ColSel = .Col
        .ScrollTrack = True
        
    End With
    
End Sub

Private Sub Grid_DblClick()
    
    With Grid
        
        Dim TmpFileName As String
        
        Select Case .Col
            
            Case KCol.ColCodCaja
                
                On Error Resume Next
                Dim Resumen As String, mRsCant As ADODB.Recordset
                'Resumen = "Cola de Pendientes de la Caja [" & Grid.TextMatrix(Fila, KCol.ColCodCaja) & "]:" & vbNewLine & vbNewLine
                Resumen = Replace(StellarMensaje(637), "$(Code)", Grid.TextMatrix(Fila, KCol.ColCodCaja)) & vbNewLine & vbNewLine
                Set mRsCant = pCn.Execute("SELECT ROUND(isNULL(COUNT(*), 0), 8, 0) AS Cant " & _
                "FROM VAD20.DBO.TR_PEND_PRODUCTOS WHERE n_Caja = '" & Grid.TextMatrix(Fila, KCol.ColCodCaja) & "'")
                Resumen = Resumen & "" & StellarMensaje(631) & ": " & mRsCant!Cant & vbNewLine & vbNewLine
                CantProd = mRsCant!Cant
                Set mRsCant = pCn.Execute("SELECT ROUND(isNULL(COUNT(*), 0), 8, 0) AS Cant " & _
                "FROM VAD20.DBO.TR_PEND_CODIGOS WHERE n_Caja = '" & Grid.TextMatrix(Fila, KCol.ColCodCaja) & "'")
                Resumen = Resumen & "" & StellarMensaje(632) & ": " & mRsCant!Cant & vbNewLine & vbNewLine
                CantCod = mRsCant!Cant
                Set mRsCant = pCn.Execute("SELECT ROUND(isNULL(COUNT(*), 0), 8, 0) AS Cant " & _
                "FROM VAD20.DBO.TR_PEND_DEPARTAMENTOS WHERE n_Caja = '" & Grid.TextMatrix(Fila, KCol.ColCodCaja) & "'")
                Resumen = Resumen & "" & StellarMensaje(633) & ": " & mRsCant!Cant & vbNewLine & vbNewLine
                CantDpt = mRsCant!Cant
                Set mRsCant = pCn.Execute("SELECT ROUND(isNULL(COUNT(*), 0), 8, 0) AS Cant " & _
                "FROM VAD20.DBO.TR_PEND_GRUPOS WHERE n_Caja = '" & Grid.TextMatrix(Fila, KCol.ColCodCaja) & "'")
                Resumen = Resumen & "" & StellarMensaje(634) & ": " & mRsCant!Cant & vbNewLine & vbNewLine
                CantGrp = mRsCant!Cant
                Set mRsCant = pCn.Execute("SELECT ROUND(isNULL(COUNT(*), 0), 8, 0) AS Cant " & _
                "FROM VAD20.DBO.TR_PEND_SUBGRUPOS WHERE n_Caja = '" & Grid.TextMatrix(Fila, KCol.ColCodCaja) & "'")
                Resumen = Resumen & "" & StellarMensaje(635) & ": " & mRsCant!Cant & vbNewLine & vbNewLine
                CantSbg = mRsCant!Cant
                'If CantProd > 0 Or CantCod > 0 Or CantDpt > 0 Or CantGrp > 0 Or CantSbg > 0 Then
                    MsgBox Resumen
                'End If
                
        End Select
        
    End With
    
End Sub

Private Sub Grid_MouseUp(Button As Integer, Shift As Integer, X As Single, Y As Single)
    Grid_EnterCell
End Sub

Private Sub Grid_SelChange()
    Grid_EnterCell
End Sub

Private Sub Grid_Click()
    
    With Grid
        
        Dim TmpFileName As String
        
        Select Case .Col
            
            Case KCol.ColInstancia, KCol.ColOptionalLogin, KCol.ColOptionalPassword
                
                'Grid_KeyPress 0
                'Grid_KeyPressEdit Grid.Row, Grid.Col, 0
                Grid.EditText = Grid.TextMatrix(Grid.Row, Grid.Col)
                SafeSendKeys "{F2}"
                If ModoTouch Then
                    DoEvents
                    txt.Text = Grid.EditText
                    Set txt.Font = Grid.Font
                    txt.Left = Frame_Detalle.Left + Grid.Left + Grid.CellLeft
                    txt.Top = Frame_Detalle.Top + Grid.Top + Grid.CellTop
                    txt.Width = Grid.CellWidth
                    txt.Height = Grid.CellHeight
                    txt.Visible = True
                    txt.ZOrder
                    Set CampoT = txt
                    TecladoPOS CampoT
                    txt_Change
                    txt.Visible = False
                End If
                
            Case KCol.ColPicEstatus
                
                Select Case True
                    
                    Case Grid.CellPicture Is CmdEdit.Picture, Grid.CellPicture Is CmdRetry.Picture
                        
                        If SilentMode Then
                            'chkUpdateRemaining.Caption = "Verificando datos en Fila " & Fila & " " & "[" & Grid.TextMatrix(Fila, KCol.ColCodCaja) & "]"
                            chkUpdateRemaining.Caption = Replace(Replace(StellarMensaje(619), "$(Fila)", Fila), "$(Code)", Grid.TextMatrix(Fila, KCol.ColCodCaja))
                            DoEvents
                        End If
                        
                        If Val(Grid.TextMatrix(Grid.Row, KCol.ColHiddenTipoPOS)) = 0 Then
                            TmpFileName = "POS_Update"
                        Else
                            TmpFileName = "FOOD_Update"
                        End If
                        
                        If Trim(Grid.TextMatrix(Grid.Row, KCol.ColInstancia)) <> Empty _
                        And Trim(Grid.TextMatrix(Grid.Row, KCol.ColOptionalLogin)) <> Empty Then
                            
                            If SilentMode Then
                                'chkUpdateRemaining.Caption = "Actualizando Fila " & Fila & " " & "[" & Grid.TextMatrix(Fila, KCol.ColCodCaja) & "]"
                                chkUpdateRemaining.Caption = Replace(Replace(StellarMensaje(620), "$(Fila)", Fila), "$(Code)", Grid.TextMatrix(Fila, KCol.ColCodCaja))
                                ScrollGrid.Value = Fila
                            End If
                            
                            'Resp1 = ShellEx(0, "open", App.Path & "\Messages\Progress.exe", _
                            "|p|Title=UpdatePOS Utility|p|Subtitle=Actualizando Caja [" & Grid.TextMatrix(Grid.Row, KCol.ColCodCaja) & "][" & Grid.TextMatrix(Grid.Row, KCol.ColInstancia) & "]", "", 1)
                            Resp1 = ShellEx(0, "open", App.Path & "\Messages\Progress.exe", _
                            "|p|Title=UpdatePOS Utility|p|Subtitle=" & Replace(Replace(StellarMensaje(625), "$(Code)", Grid.TextMatrix(Fila, KCol.ColCodCaja)), "$(Instance)", Grid.TextMatrix(Fila, KCol.ColInstancia)), "", 1)
                            Resp3 = ShellEx(0, "open", App.Path & "\Messages\SendMessage_RequiresParams.exe", _
                            "..." & StellarMensaje(610) & "...", "", 1) 'por favor espere
                            
                            RespScript = ShellAndWait(App.Path & "\" & TmpFileName & ".bat" & _
                            " " & Grid.TextMatrix(Grid.Row, KCol.ColInstancia) & " " & Grid.TextMatrix(Grid.Row, KCol.ColOptionalLogin) & " " & """" & Grid.TextMatrix(Grid.Row, KCol.ColOptionalPassword) & """" & _
                            " " & FrmServerConnection.txtServer & " " & Grid.TextMatrix(Grid.Row, KCol.ColCodCaja) & _
                            " " & FrmServerConnection.txtUsuario & " " & """" & FrmServerConnection.txtClave & """", vbHide)
                            Sleep 200
                            
                            ErrorLevel = Val(LoadTextFile(App.Path & "\ERRORLEVEL.TXT"))
                            
                            Success = (ErrorLevel = 0)
                            
                            KillSecure App.Path & "\ERRORLEVEL.TXT"
                            
                            Grid.Col = KCol.ColPicEstatus
                            
                            RespFin = ShellEx(0, "open", App.Path & "\Messages\SendMessage_RequiresParams.exe", _
                            "[StellarInstallerExternalProgressID SAFE APP CLOSE]", "", 1)
                            If PuedeObtenerFoco(Me) Then Me.SetFocus
                            SetWindowPos Me.hWnd, 0, 0, 0, 0, 0, _
                            (SWP_SHOWWINDOW & SWP_NOMOVE & SWP_NOREPOSITION & SWP_NOREDRAW & SWP_NOSIZE)
                            
                            If Success Then
                                Set Grid.CellPicture = CmdOK.Picture
                            Else
                                Set Grid.CellPicture = CmdRetry.Picture
                            End If
                            
                        Else
                            If Not SilentMode Then
                                MsgBox StellarMensaje(621) '"Debe establecer la IP o Nombre de Instancia antes de proceder a actualizar los datos de la caja. Aseg�rese de que el servicio SQL y la caja este accesible a nivel de red."
                            End If
                        End If
                        
                    Case (Grid.CellPicture Is CmdOK.Picture And Not SilentMode)
                        
                        If Val(Grid.TextMatrix(Grid.Row, KCol.ColHiddenTipoPOS)) = 0 Then
                            TmpFileName = "POS_Update"
                        Else
                            TmpFileName = "FOOD_Update"
                        End If
                        
                        'If MsgBox("Ya la caja fue actualizada, �esta seguro de volver a ejecutar el proceso?", vbYesNo) = vbYes Then
                        If MsgBox(StellarMensaje(622), vbYesNo) = vbYes Then
                            
                            If Trim(Grid.TextMatrix(Grid.Row, KCol.ColInstancia)) <> Empty _
                            And Trim(Grid.TextMatrix(Grid.Row, KCol.ColOptionalLogin)) <> Empty Then
                                
                                'Resp1 = ShellEx(0, "open", App.Path & "\Messages\Progress.exe", _
                                "|p|Title=UpdatePOS Utility|p|Subtitle=Actualizando Caja [" & Grid.TextMatrix(Grid.Row, KCol.ColCodCaja) & "][" & Grid.TextMatrix(Grid.Row, KCol.ColInstancia) & "]", "", 1)
                                Resp1 = ShellEx(0, "open", App.Path & "\Messages\Progress.exe", _
                                "|p|Title=UpdatePOS Utility|p|Subtitle=" & Replace(Replace(StellarMensaje(625), "$(Code)", Grid.TextMatrix(Fila, KCol.ColCodCaja)), "$(Instance)", Grid.TextMatrix(Fila, KCol.ColInstancia)), "", 1)
                                Resp3 = ShellEx(0, "open", App.Path & "\Messages\SendMessage_RequiresParams.exe", _
                                "..." & StellarMensaje(610) & "...", "", 1) 'por favor espere
                                
                                RespScript = ShellAndWait(App.Path & "\" & TmpFileName & ".bat" & _
                                " " & Grid.TextMatrix(Grid.Row, KCol.ColInstancia) & " " & Grid.TextMatrix(Grid.Row, KCol.ColOptionalLogin) & " " & """" & Grid.TextMatrix(Grid.Row, KCol.ColOptionalPassword) & """" & _
                                " " & FrmServerConnection.txtServer & " " & Grid.TextMatrix(Grid.Row, KCol.ColCodCaja) & _
                                " " & FrmServerConnection.txtUsuario & " " & """" & FrmServerConnection.txtClave & """", vbHide)
                            
                                Sleep 200
                                
                                ErrorLevel = Val(LoadTextFile(App.Path & "\ERRORLEVEL.TXT"))
                                
                                Success = (ErrorLevel = 0)
                                
                                KillSecure App.Path & "\ERRORLEVEL.TXT"
                                
                                Grid.Col = KCol.ColPicEstatus
                                
                                RespFin = ShellEx(0, "open", App.Path & "\Messages\SendMessage_RequiresParams.exe", "[StellarInstallerExternalProgressID SAFE APP CLOSE]", "", 1)
                                If PuedeObtenerFoco(Me) Then Me.SetFocus
                                SetWindowPos Me.hWnd, 0, 0, 0, 0, 0, _
                                (SWP_SHOWWINDOW & SWP_NOMOVE & SWP_NOREPOSITION & SWP_NOREDRAW & SWP_NOSIZE)
                                
                                If Success Then
                                    Set Grid.CellPicture = CmdOK.Picture
                                    MsgBox StellarMensaje(623) '"La caja fue actualizada exitosamente."
                                Else
                                    Set Grid.CellPicture = CmdRetry.Picture
                                    MsgBox StellarMensaje(624) '"No fue posible realizar la actualizaci�n."
                                End If
                                
                            Else
                                MsgBox StellarMensaje(621) '"Debe establecer la IP o Nombre de Instancia antes de proceder a actualizar los datos de la caja. Aseg�rese de que el servicio SQL y la caja este accesible a nivel de red"
                            End If
                            
                        End If
                        
                End Select
                
            Case Else
        
        End Select
    
    End With
    
End Sub

Private Sub Grid_EnterCell()
    
    On Error Resume Next
    
    Select Case Grid.Col
        
        Case Is = KCol.ColCodCaja, KCol.ColDescCaja, KCol.ColHiddenTipoPOS, _
        KCol.ColPicEstatus, KCol.ColHiddenAux
            Grid.Editable = False
        Case Else
            Grid.Editable = True
        
    End Select
    
    If Grid.Rows = 1 Then
        FrameOK.Visible = False
        FrameRetry.Visible = False
        FrameEdit.Visible = False
        Exit Sub
    End If
    
    If Not Band Then
        
        If Fila <> Grid.Row Then
            
            If Fila > 0 Then
                Grid.RowHeight(Fila) = 600
            End If
            
            Grid.RowHeight(Grid.Row) = 720
            Fila = Grid.Row
            
        End If
    
        'MostrarEditorTexto2 Me, Grid, CmdEdit, mRowCell, mColCell
        
        If Not SilentMode Then
            Grid.ColSel = 0
            If PuedeObtenerFoco(Grid) Then Grid.SetFocus
        End If
    Else
        Band = False
    End If

End Sub

Private Sub Grid_Keydown(KeyCode As Integer, Shift As Integer)
    
    If Shift = vbCtrlMask And KeyCode = vbKeyC Then
        CtrlC Grid.TextMatrix(Grid.Row, Grid.Col)
    ElseIf Shift = vbCtrlMask And KeyCode = vbKeyV Then
        Select Case Grid.Col
            Case KCol.ColInstancia, KCol.ColOptionalLogin, KCol.ColOptionalPassword
                Grid.TextMatrix(Grid.Row, Grid.Col) = CtrlV
                Grid_Click
        End Select
    End If
    
End Sub

Private Sub DeleteCurrentRow()
    If (Grid.Rows > 2) Then
        Grid.RemoveItem (Grid.Row)
    Else
        Grid.Rows = Grid.Rows - 1
        Grid.Rows = Grid.Rows + 1
    End If
End Sub

Private Sub Grid_KeyPress(KeyAscii As Integer)
    
    With Grid
    
        Select Case .Col
        
            Case Is = KCol.ColInstancia, KCol.ColOptionalLogin, KCol.ColOptionalPassword
        
                'Select Case KeyAscii
        
                    'Case 46, 48 To 57
        
                        Grid.Editable = True
        
                    'Case Else
        
                        'Grid.Editable = False
        
                'End Select
        
            Case Else
            
                Grid.Editable = False
                
        End Select
    
    End With

End Sub

Private Sub Grid_KeyPressEdit(ByVal Row As Long, ByVal Col As Long, KeyAscii As Integer)
    
    If KeyAscii = 3 Or KeyAscii = 22 Then ' Para evitar que interfiera con el Ctrl C / Ctrl V
        KeyAscii = 0
        Exit Sub
    End If
    
    Select Case Col
    
        Case Is = KCol.ColInstancia, KCol.ColOptionalLogin, KCol.ColOptionalPassword
    
'            Select Case KeyAscii
'
'                Case 46, 48 To 57, 8, 13, 27
'
'                    KeyAscii = KeyAscii
'
'                    If Col = KlexGridCols.ColMontoFijo And KeyAscii = 13 And Row = Grid.Rows - 1 Then
'                        Command1_Click
'                    End If
'
'                Case Else
'
'                    KeyAscii = 0
'
'            End Select
    
    End Select

End Sub

Private Sub Grid_ValidateEdit(Row As Long, Col As Long, Cancel As Boolean)
    
    Select Case Col
    
        Case Is = KCol.ColInstancia, KCol.ColOptionalLogin, KCol.ColOptionalPassword
            
            ValorEntrada = Grid.TextMatrix(Row, Col)
            Grid.EditText = ValorEntrada
            
'            If IsNumeric(ValorEntrada) Then
'
'                Grid.TextMatrix(Row, Col) = FormatNumber(ValorEntrada, 2)
'                Grid.EditText = FormatNumber(ValorEntrada, 2)
'
'                If Col = KlexGridCols.ColPorcentaje Then
'                    If CDbl(ValorEntrada) >= 100 Then
'                        Grid.TextMatrix(Row, Col) = FormatNumber(100, 2)
'                        Grid.EditText = FormatNumber(100, 2)
'                    End If
'                End If
'
'            Else
'
'                Grid.TextMatrix(Row, Col) = ""
'                Grid.EditText = ""
'
'            End If
'
'            If Len(ValorEntrada) > 20 Then
'                Grid.TextMatrix(Row, Col) = ""
'                Grid.EditText = ""
'            End If
                
    End Select

End Sub

Private Sub ScrollGrid_Change()
    On Error GoTo ErrScroll
    If ScrollGrid.Value <> Grid.Row Or SilentMode Then
        Grid.TopRow = ScrollGrid.Value
        Grid.Row = ScrollGrid.Value
        If Not SilentMode Then
            If PuedeObtenerFoco(Grid) Then Grid.SetFocus
        End If
    End If
    Exit Sub
ErrScroll:
    Err.Clear
End Sub

Private Sub ScrollGrid_Scroll()
    'Debug.Print ScrollGrid.value
    ScrollGrid_Change
End Sub

Private Sub txt_Change()
    Grid.EditText = txt.Text
End Sub
