SETLOCAL ENABLEDELAYEDEXPANSION

IF DEFINED PROGRAMW6432 (GOTO CHECK) ELSE (GOTO WIN32)

:CHECK

IF EXIST "%PROGRAMW6432%\Microsoft SQL Server\Client SDK\ODBC\130\Tools\Binn\SQLCMD.EXE" (
  GOTO SQLCMD64
)

IF EXIST "%PROGRAMW6432%\Microsoft SQL Server\Client SDK\ODBC\110\Tools\Binn\SQLCMD.EXE" (
  GOTO SQLCMD64
)

IF EXIST "%PROGRAMW6432%\Microsoft SQL Server\110\Tools\Binn\SQLCMD.EXE" (
  GOTO SQLCMD64
)

GOTO SQLCMD32

:WIN32

IF EXIST "%PROGRAMFILES%\Microsoft SQL Server\Client SDK\ODBC\130\Tools\Binn\SQLCMD.EXE" (
  
  START /b /WAIT "" "%PROGRAMFILES%\Microsoft SQL Server\Client SDK\ODBC\130\Tools\Binn\SQLCMD.EXE" -b -m-1 -V1 -S "%1" -U "%2" -P %3 -e -i "%~dp0FOOD_Update.sql" -v SERVERNAME = "%4" -v POSNUMBER = "%5" -v LNAME = "%6" -v KEY = %7
  
  ECHO !ERRORLEVEL! > "%~dp0ERRORLEVEL.txt"
  
  GOTO WIN32_CONTINUE
  
)

IF EXIST "%PROGRAMFILES%\Microsoft SQL Server\Client SDK\ODBC\110\Tools\Binn\SQLCMD.EXE" (
  
  START /b /WAIT "" "%PROGRAMFILES%\Microsoft SQL Server\Client SDK\ODBC\110\Tools\Binn\SQLCMD.EXE" -b -m-1 -V1 -S "%1" -U "%2" -P %3 -e -i "%~dp0FOOD_Update.sql" -v SERVERNAME = "%4" -v POSNUMBER = "%5" -v LNAME = "%6" -v KEY = %7
  
  ECHO !ERRORLEVEL! > "%~dp0ERRORLEVEL.txt"
  
  GOTO WIN32_CONTINUE
  
)

IF EXIST "%PROGRAMFILES%\Microsoft SQL Server\110\Tools\Binn\SQLCMD.EXE" (

  START /b /WAIT "" "%PROGRAMFILES%\Microsoft SQL Server\110\Tools\Binn\SQLCMD.EXE" -b -m-1 -V1 -S "%1" -U "%2" -P %3 -e -i "%~dp0FOOD_Update.sql" -v SERVERNAME = "%4" -v POSNUMBER = "%5" -v LNAME = "%6" -v KEY = %7
  
  ECHO !ERRORLEVEL! > "%~dp0ERRORLEVEL.txt"
  
  GOTO WIN32_CONTINUE

)

:WIN32_CONTINUE

GOTO END

:SQLCMD64

IF EXIST "%PROGRAMW6432%\Microsoft SQL Server\Client SDK\ODBC\130\Tools\Binn\SQLCMD.EXE" (
  
  START /b /WAIT "" "%PROGRAMW6432%\Microsoft SQL Server\Client SDK\ODBC\130\Tools\Binn\SQLCMD.EXE" -b -m-1 -V1 -S "%1" -U "%2" -P %3 -e -i "%~dp0FOOD_Update.sql" -v SERVERNAME = "%4" -v POSNUMBER = "%5" -v LNAME = "%6" -v KEY = %7
  
  ECHO !ERRORLEVEL! > "%~dp0ERRORLEVEL.txt"
  
  REM PAUSE
  
  GOTO SQLCMD64_CONTINUE

)

IF EXIST "%PROGRAMW6432%\Microsoft SQL Server\Client SDK\ODBC\110\Tools\Binn\SQLCMD.EXE" (
  
  START /b /WAIT "" "%PROGRAMW6432%\Microsoft SQL Server\Client SDK\ODBC\110\Tools\Binn\SQLCMD.EXE" -b -m-1 -V1 -S "%1" -U "%2" -P %3 -e -i "%~dp0FOOD_Update.sql" -v SERVERNAME = "%4" -v POSNUMBER = "%5" -v LNAME = "%6" -v KEY = %7
  
  ECHO !ERRORLEVEL! > "%~dp0ERRORLEVEL.txt"
  
  REM PAUSE
  
  GOTO SQLCMD64_CONTINUE

)

IF EXIST "%PROGRAMW6432%\Microsoft SQL Server\110\Tools\Binn\SQLCMD.EXE" (

  START /b /WAIT "" "%PROGRAMW6432%\Microsoft SQL Server\110\Tools\Binn\SQLCMD.EXE" -b -m-1 -V1 -S "%1" -U "%2" -P %3 -e -i "%~dp0FOOD_Update.sql" -v SERVERNAME = "%4" -v POSNUMBER = "%5" -v LNAME = "%6" -v KEY = %7
  
  ECHO !ERRORLEVEL! > "%~dp0ERRORLEVEL.txt"
  
  GOTO SQLCMD64_CONTINUE

)

:SQLCMD64_CONTINUE

GOTO END

:SQLCMD32

IF EXIST "%PROGRAMW6432%\Microsoft SQL Server\Client SDK\ODBC\130\Tools\Binn\SQLCMD.EXE" (
  
  START /b /WAIT "" "%PROGRAMW6432%\Microsoft SQL Server\Client SDK\ODBC\110\Tools\Binn\SQLCMD.EXE" -b -m-1 -V1 -S "%1" -U "%2" -P %3 -e -i "%~dp0FOOD_Update.sql" -v SERVERNAME = "%4" -v POSNUMBER = "%5" -v LNAME = "%6" -v KEY = %7
  
  ECHO !ERRORLEVEL! > "%~dp0ERRORLEVEL.txt"
  
  REM PAUSE
  
  GOTO SQLCMD64_CONTINUE

)

IF EXIST "%PROGRAMFILES(X86)%\Microsoft SQL Server\Client SDK\ODBC\110\Tools\Binn\SQLCMD.EXE" (

  START /b /WAIT "" "%PROGRAMFILES(X86)%\Microsoft SQL Server\Client SDK\ODBC\110\Tools\Binn\SQLCMD.EXE" -b -m-1 -V1 -S "%1" -U "%2" -P %3 -e -i "%~dp0FOOD_Update.sql" -v SERVERNAME = "%4" -v POSNUMBER = "%5" -v LNAME = "%6" -v KEY = %7
  
  ECHO !ERRORLEVEL! > "%~dp0ERRORLEVEL.txt"
  
  GOTO SQLCMD32_CONTINUE

)

IF EXIST "%PROGRAMFILES(X86)%\Microsoft SQL Server\110\Tools\Binn\SQLCMD.EXE" (

  START /b /WAIT "" "%PROGRAMFILES(X86)%\Microsoft SQL Server\110\Tools\Binn\SQLCMD.EXE" -b -m-1 -V1 -S "%1" -U "%2" -P %3 -e -i "%~dp0FOOD_Update.sql" -v SERVERNAME = "%4" -v POSNUMBER = "%5" -v LNAME = "%6" -v KEY = %7
  
  ECHO !ERRORLEVEL! > "%~dp0ERRORLEVEL.txt"
  
  GOTO SQLCMD32_CONTINUE

)

:SQLCMD32_CONTINUE

GOTO END

:END 

REM SEGUIR AQUI... O TERMINAR.