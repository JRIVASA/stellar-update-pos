Attribute VB_Name = "Mod_FuncionesGenerales"
Option Explicit

Public DebugVBApp As Boolean
Public Const TH32CS_SNAPPROCESS As Long = 2&
Public Const MAX_PATH As Long = 260

Public Type ProcessEntry32
    dwSize As Long
    cntUsage As Long
    th32ProcessID As Long
    th32DefaultHeapID As Long
    th32ModuleID As Long
    cntThreads As Long
    th32ParentProcessID As Long
    pcPriClassBase As Long
    dwFlags As Long
    szExeFile As String * MAX_PATH
End Type

Public AppProcId As Long
Public AppProcInfo As ProcessEntry32

Public Declare Function GetCurrentProcessId Lib "kernel32" () As Long

Public Declare Function CreateToolhelp32Snapshot Lib "kernel32" _
(ByVal lFlags As Long, ByVal lProcessID As Long) As Long

Public Declare Function ProcessFirst Lib "kernel32" _
Alias "Process32First" _
(ByVal hSnapShot As Long, uProcess As ProcessEntry32) As Long

Public Declare Function ProcessNext Lib "kernel32" _
Alias "Process32Next" _
(ByVal hSnapShot As Long, uProcess As ProcessEntry32) As Long

Public Declare Sub CloseHandle Lib "kernel32" _
(ByVal hPass As Long)

Private Const VK_DOWN As Long = &H28
Private Const WM_SYSKEYDOWN As Long = &H104

Public Declare Sub Sleep Lib "kernel32" (ByVal dwMilliseconds As Long)

Private Declare Function SendMessage Lib "user32.dll" _
Alias "SendMessageA" ( _
ByVal hWnd As Long, _
ByVal wMsg As Long, _
ByVal wParam As Long, _
ByVal lParam As Any) As Long
                         
Public Declare Sub ReleaseCapture Lib "user32" ()
                         
Private Declare Function ShellExecute Lib "shell32.dll" Alias "ShellExecuteA" ( _
ByVal hWnd As Long, _
ByVal lpOperation As String, _
ByVal lpFile As String, _
ByVal lpParameters As String, _
ByVal lpDirectory As String, _
ByVal nShowCmd As Long) As Long

Private Type ShellExecuteInfo
    cbSize As Long
    fMask As Long
    hWnd As Long
    lpVerb As String
    lpFile As String
    lpParameters As String
    lpDirectory As String
    nShow As Long
    hInstApp As Long
    lpIDList As Long
    lpClass As String
    hkeyClass As Long
    dwHotKey As Long
    hIcon As Long
    hProcess As Long
End Type

Private Declare Function ShellExecuteEx Lib "shell32.dll" ( _
ByRef lpExecInfo As ShellExecuteInfo) As Long

'**************************************
'Windows API/Global Declarations for :Changing priority
'**************************************

Private Const NORMAL_PRIORITY_CLASS = &H20
Private Const IDLE_PRIORITY_CLASS = &H40
Private Const HIGH_PRIORITY_CLASS = &H80
Private Const REALTIME_PRIORITY_CLASS = &H100
Private Const PROCESS_DUP_HANDLE = &H40

Private Const SYNCHRONIZE = &H100000
Private Const INFINITE = -1&

Private Declare Function OpenProcess Lib "kernel32" (ByVal dwDesiredAccess As Long, _
ByVal bInheritHandle As Long, ByVal dwProcessID As Long) As Long
Private Declare Function WaitForSingleObject Lib "kernel32" (ByVal hHandle As Long, ByVal dwMilliseconds As Long) As Long

Private Declare Function SetPriorityClass& Lib "kernel32" (ByVal hProcess As Long, _
ByVal dwPriorityClass As Long)

Private Const SND_APPLICATION = &H80         '  look for application specific association
Private Const SND_ALIAS = &H10000     '  name is a WIN.INI [sounds] entry
Private Const SND_ALIAS_ID = &H110000    '  name is a WIN.INI [sounds] entry identifier
Private Const SND_ASYNC = &H1         '  play asynchronously
Private Const SND_FILENAME = &H20000     '  name is a file name
Private Const SND_LOOP = &H8         '  loop the sound until next sndPlaySound
Private Const SND_MEMORY = &H4         '  lpszSoundName points to a memory file
Private Const SND_NODEFAULT = &H2         '  silence not default, if sound not found
Private Const SND_NOSTOP = &H10        '  don't stop any currently playing sound
Private Const SND_NOWAIT = &H2000      '  don't wait if the driver is busy
Private Const SND_PURGE = &H40               '  purge non-static events for task
Private Const SND_RESOURCE = &H40004     '  name is a resource name or atom
Private Const SND_SYNC = &H0         '  play synchronously (default)

Private Declare Function PlaySound Lib "winmm.dll" Alias "PlaySoundA" _
(ByVal lpszName As String, ByVal hModule As Long, ByVal dwFlags As Long) As Long

Declare Function MCISendString Lib "winmm.dll" Alias "mciSendStringA" (ByVal lpstrCommand As String, _
ByVal lpstrReturnString As String, ByVal uReturnLength As Long, ByVal hwndCallback As Long) As Long

Declare Function MCIGetErrorString Lib "winmm.dll" Alias "mciGetErrorStringA" (ByVal dwError As Long, _
ByVal lpstrBuffer As String, ByVal uLength As Long) As Long

Public Declare Function FindWindow Lib "user32.dll" Alias "FindWindowA" _
(ByVal lpClassName As String, ByVal lpCaption As Any) As Integer

Public Declare Function IsWindowVisible Lib "user32" _
(ByVal hWnd As Long) As Long

Public Declare Function ShowWindow Lib "user32" _
(ByVal hWnd As Long, ByVal nCmdShow As Long) As Long

Private Declare Function SetForegroundWindow Lib "user32" (ByVal hWnd As Long) As Long
   
Private Declare Function BringWindowToTop Lib "user32" (ByVal hWnd As Long) As Long
   
Private Declare Function WindowSetFocus Lib "user32" Alias "SetFocus" (ByVal hWnd As Long) As Long

Public Declare Function SetFocus Lib "user32" (ByVal hWnd As Long) As Long

' GetWindow() Constants
Private Const GW_HWNDFIRST = 0&
Private Const GW_HWNDNEXT = 2&
Private Const GW_CHILD = 5&

Public Const SWP_NOSIZE = &H1
Public Const SWP_NOMOVE = &H2
Public Const SWP_NOZORDER = &H4
Public Const SWP_NOREDRAW = &H8
Public Const SWP_NOACTIVATE = &H10
Public Const SWP_FRAMECHANGED = &H20        '  frame change send WM_NCCALCSIZE
Public Const SWP_SHOWWINDOW = &H40
Public Const SWP_HIDEWINDOW = &H80
Public Const SWP_NOCOPYBITS = &H100
Public Const SWP_NOOWNERZORDER = &H200      '  No use propietary Z order

Public Const SWP_DRAWFRAME = SWP_FRAMECHANGED
Public Const SWP_NOREPOSITION = SWP_NOOWNERZORDER

Public Const HWND_TOP = 0
Public Const HWND_BOTTOM = 1
Public Const HWND_TOPMOST = -1
Public Const HWND_NOTOPMOST = -2

Private Type Rect
    Left As Long
    Top As Long
    Right As Long
    Bottom As Long
End Type

Private Declare Function GetWindowTextLength Lib "user32" Alias "GetWindowTextLengthA" _
(ByVal hWnd As Long) As Long

Private Declare Function GetWindowRect Lib "user32" (ByVal hWnd As Long, _
    lpRect As Rect) As Long
 
' get a window's size in pixel
 
Public Declare Function SetWindowPos Lib "user32" (ByVal hWnd As Long, _
ByVal hWndInsertAfter As Long, ByVal X As Long, ByVal Y As Long, _
ByVal cx As Long, ByVal cy As Long, ByVal wFlags As Long) As Long
 
Private Declare Sub mouse_event Lib "user32" (ByVal dwFlags As Long, ByVal dx As Long, ByVal dy As Long, ByVal cButtons As Long, ByVal dwExtraInfo As Long)
Private Declare Function LoadCursor Lib "user32" Alias "LoadCursorA" (ByVal hInstance As Long, ByVal lpCursorName As Long) As Long
Private Declare Function SetCursor Lib "user32" (ByVal hCursor As Long) As Long

Private Const MOUSEEVENTF_LEFTUP = &H4 '  left button up
Private Const MOUSEEVENTF_LEFTDOWN = &H2 '  left button down
Private Const IDC_ARROW = 32512&
Private Const IDC_HAND = 32649&
 
Public Declare Function GetDesktopWindow Lib "user32" () As Long
Private Declare Function GetWindow Lib "user32" (ByVal hWnd As Long, ByVal wCmd As Long) As Long
Private Declare Function GetWindowText Lib "user32" Alias "GetWindowTextA" (ByVal hWnd As Long, ByVal lpString As String, ByVal cch As Long) As Long
Private Declare Function GetClassName Lib "user32" Alias "GetClassNameA" (ByVal hWnd As Long, ByVal lpClassName As String, ByVal nMaxCount As Long) As Long
Private Declare Function GetParent Lib "user32" (ByVal hWnd As Long) As Long
Private Declare Function GetWindowWord Lib "user32" (ByVal hWnd As Long, ByVal nIndex As Long) As Integer

Declare Function EnumWindows Lib "user32" _
   (ByVal lpEnumFunc As Long, ByVal lParam As Long) As Long

Declare Function EnumChildWindows Lib "user32" (ByVal hWndParent _
   As Long, ByVal lpEnumFunc As Long, ByVal lParam As Long) As Long
   
Private Const CB_ERR = (-1)

Private Const CB_GETCOUNT = &H146
Private Const CB_GETLBTEXTLEN = &H149
Private Const CB_GETLBTEXT = &H148

Private Const CB_FINDSTRING = &H14C
Private Const CB_FINDSTRINGEXACT = &H158
Private Const CB_SETCURSEL = &H14E

Const WM_NCLBUTTONDOWN = &HA1
Const HTCAPTION = 2
 
Public Type NotifyIconData
    cbSize As Long
    hWnd As Long
    uId As Long
    uFlags As Long
    uCallBackMessage As Long
    hIcon As Long
    szTip As String * 64
End Type

Public Const NIM_ADD = &H0
Public Const NIM_MODIFY = &H1
Public Const NIM_DELETE = &H2
Public Const WM_MOUSEMOVE = &H200
Public Const NIF_MESSAGE = &H1
Public Const NIF_ICON = &H2
Public Const NIF_TIP = &H4
Public Const WM_LBUTTONDBLCLK = &H203 'Double-click
Public Const WM_LBUTTONDOWN = &H201 'Button down
Public Const WM_LBUTTONUP = &H202 'Button up
Public Const WM_RBUTTONDBLCLK = &H206 'Double-click
Public Const WM_RBUTTONDOWN = &H204 'Button down
Public Const WM_RBUTTONUP = &H205 'Button up

Public Declare Function Shell_NotifyIcon Lib "shell32" Alias "Shell_NotifyIconA" (ByVal dwMessage As Long, pnid As NotifyIconData) As Boolean

Public TrayInfo As NotifyIconData

Public Declare Function GetAsyncKeyState Lib "user32.dll" (ByVal vbKey As Long) As Integer

Public Declare Function GetWindowLong Lib "user32" Alias "GetWindowLongA" (ByVal hWnd As Long, ByVal nIndex As Long) As Long
Public Declare Function SetWindowLong Lib "user32" Alias "SetWindowLongA" (ByVal hWnd As Long, ByVal nIndex As Long, ByVal dwNewLong As Long) As Long

Public Const GWL_EXSTYLE = (-20)
Public Const WS_EX_APPWINDOW = &H40000

Const FLASHW_STOP = 0 'Stop flashing. The system restores the window to its original state.
Const FLASHW_CAPTION = &H1 'Flash the window caption.
Const FLASHW_TRAY = &H2 'Flash the taskbar button.
Const FLASHW_ALL = (FLASHW_CAPTION Or FLASHW_TRAY) 'Flash both the window caption and taskbar button. This is equivalent to setting the FLASHW_CAPTION Or FLASHW_TRAY flags.
Const FLASHW_TIMER = &H4 'Flash continuously, until the FLASHW_STOP flag is set.
Const FLASHW_TIMERNOFG = &HC 'Flash continuously until the window comes to the foreground.

Private Type FlashWInfo
    cbSize As Long
    hWnd As Long
    dwFlags As Long
    uCount As Long
    dwTimeout As Long
End Type

Private Declare Function FlashWindowEx Lib "user32" (pfwi As FlashWInfo) As Boolean
Private Declare Function FlashWindow Lib "user32" (ByVal hWnd As Long, ByVal bInvert As Long) As Long
Private Declare Function GetActiveWindow Lib "user32" () As Long

Public TrayedForms          As Collection
Public CallerAppForms       As Collection
Public CallerAppIcon        As Object
Public CallerAppCaption     As String

Public TempDisableHotkeys   As Boolean

Function FindWindowLike(ByVal WindowText As String, ByVal ClassName As String, ByRef alMatchingHwnds() As Long, Optional ByVal lStartHwnd As Long = 0, Optional ByVal lChildID As Long = -1) As Integer

    Const GW_HWNDNEXT As Long = 2
    Const GW_CHILD As Long = 5
    Const GWW_ID As Long = (-12)

    Dim sWindowText As String, sClassname As String
    Dim lWinID As Long, lhWnd As Long
    Dim lRetVal As Long
    'Hold the Level of recursion
    Static slLevel As Long
    'Hold the number of matching windows
    Static slFound As Long

    'Initialize if necessary
    If slLevel = 0 Then
        slFound = 0
        ReDim alMatchingHwnds(0 To 1)
        If lStartHwnd = 0 Then
            lStartHwnd = GetDesktopWindow()
        End If
    End If

    'Increase recursion counter
    slLevel = slLevel + 1

    'Get first child window
    lhWnd = GetWindow(lStartHwnd, GW_CHILD)

    Do Until lhWnd = 0
        'Search children by recursion
        lRetVal = FindWindowLike(WindowText, ClassName, alMatchingHwnds(), lhWnd, lChildID)

        'Get the window text and class name
        sWindowText = Space(255)
        lRetVal = GetWindowText(lhWnd, sWindowText, 255)
        sWindowText = Left$(sWindowText, lRetVal)
        sClassname = Space(255)
        lRetVal = GetClassName(lhWnd, sClassname, 255)
        sClassname = Left$(sClassname, lRetVal)

        'If window is a child get the ID
        If GetParent(lhWnd) <> 0 Then
            lRetVal = GetWindowWord(lhWnd, GWW_ID)
            lWinID = CLng("&H" & Hex(lRetVal))
        Else
            lWinID = -1
        End If

        'Check that window matches the search parameters
        If (sWindowText Like WindowText Or WindowText = "") And (sClassname Like ClassName Or ClassName = "") Then
            If lChildID = -1 Then
                'Found a match, increment counter and add handle to array
                slFound = slFound + 1
                ReDim Preserve alMatchingHwnds(0 To slFound)
                alMatchingHwnds(slFound) = lhWnd
            ElseIf lWinID <> -1 Then
                If lWinID = lChildID Then
                    'Found a match, increment counter and add handle to array
                    slFound = slFound + 1
                    ReDim Preserve alMatchingHwnds(0 To slFound)
                    alMatchingHwnds(slFound) = lhWnd
                End If
            End If
        End If

        'Get next child window
        lhWnd = GetWindow(lhWnd, GW_HWNDNEXT)
    Loop

    'Decrement recursion counter
    slLevel = slLevel - 1

    'Return the number of windows found
    FindWindowLike = slFound
    
End Function
 
Public Function ShellEx(ByVal hWnd As Long, ByVal lpOperation As String, _
ByVal lpFile As String, ByVal lpParameters As String, _
ByVal lpDirectory As String, ByVal nShowCmd As Long) As Long
    ShellEx = ShellExecute(hWnd, lpOperation, lpFile, lpParameters, lpDirectory, nShowCmd)
End Function

Public Function ShellExInfo(ByVal hWnd As Long, ByVal lpOperation As String, _
ByVal lpFile As String, ByVal lpParameters As String, _
ByVal lpDirectory As String, ByVal nShowCmd As Long) As Long
    
    Dim SxInfo As ShellExecuteInfo
    
    With SxInfo
        .hWnd = hWnd
        .lpVerb = lpOperation
        .lpFile = lpFile
        .lpParameters = lpParameters
        .lpDirectory = lpDirectory
        .nShow = nShowCmd
        .fMask = &H40
        ShellExInfo = ShellExecuteEx(SxInfo)
        ShellExInfo = .hProcess
    End With
    
End Function

Public Sub HandCursor()
    On Error Resume Next
    SetCursor LoadCursor(0, IDC_HAND)
    mouse_event MOUSEEVENTF_LEFTDOWN, 0, 0, 0, 0
End Sub

Public Sub ArrowCursor()
    On Error Resume Next
    SetCursor LoadCursor(0, IDC_ARROW)
    mouse_event MOUSEEVENTF_LEFTUP, 0, 0, 0, 0
End Sub

Function GetWindowSize(hWnd As Long) As Collection
    
    On Error GoTo ErrWS
    
    Dim WindowRect As Rect, RectObj As New Collection
    
    GetWindowRect hWnd, WindowRect
    
    RectObj.Add (WindowRect.Right - WindowRect.Left), "Width"
    RectObj.Add (WindowRect.Bottom - WindowRect.Top), "Height"
    
    Set GetWindowSize = RectObj
    
    Exit Function
    
ErrWS:
    
    Set GetWindowSize = Nothing
    
End Function

Private Function WindowTitle(ByVal hWnd As Long) As String
    ' Devuelve el t�tulo de una ventana, seg�n el hWnd indicado.
    
    Dim sTitulo As String
    Dim LenTitulo As Long
    Dim Ret As Long
    
    'Leer la longitud del t�tulo de la ventana
    LenTitulo = GetWindowTextLength(hWnd)
    
    If LenTitulo > 0 Then
        
        LenTitulo = LenTitulo + 1
        
        sTitulo = String$(LenTitulo, 0)
        
        'Leer el t�tulo de la ventana
        
        Ret = GetWindowText(hWnd, sTitulo, LenTitulo)
        
        WindowTitle = Left$(sTitulo, Ret)
        
    End If
    
End Function

Public Function ClassName(ByVal Title As String) As String
    
    ' Devuelve el ClassName de una ventana, indicando el t�tulo de la misma.
    
    Dim hWnd As Long
    Dim sClassname As String
    Dim nMaxCount As Long
    
    hWnd = FindWindow(sClassname, Title)
    
    nMaxCount = 256
    sClassname = Space$(nMaxCount)
    nMaxCount = GetClassName(hWnd, sClassname, nMaxCount)
    ClassName = Left$(sClassname, nMaxCount)
    
End Function

Public Function FindKBDHandle() As Long
    
    'FindKBDHandle = FindWindow("OSKMainClass", "Teclado en pantalla")
    
    Dim sTitulo As String
    Dim hWnd As Long
    
    ' Primera ventana
    hWnd = GetWindow(GetDesktopWindow(), GW_CHILD)
    
    ' Recorrer el resto de las ventanas.
    
    Do While hWnd <> 0&
        
        'Si la ventana es visible
        
        If IsWindowVisible(hWnd) Then
            'Leer el caption de la ventana
            sTitulo = WindowTitle(hWnd)
            If Len(sTitulo) Then
                If UCase(ClassName(sTitulo)) = UCase("OskMainClass") Then
                    FindKBDHandle = hWnd
                    Exit Function
                End If
            End If
        End If
        
        'Siguiente ventana
        hWnd = GetWindow(hWnd, GW_HWNDNEXT)
        
    Loop
    
End Function

Public Function FindHandle(pClassName As String, Optional pTitle As String = "") As Long
    FindHandle = FindWindow(pClassName, pTitle)
End Function

Public Sub PosicionarVentana(ByVal hWnd As Long, ByVal pLeft As Long, ByVal pTop As Long)
    'On Error Resume Next
    SetWindowPos hWnd, 0, pLeft, pTop, 0, 0, SWP_NOSIZE Or SWP_NOZORDER
End Sub

'Public Sub MostrarCalendario(pDatePicker As DTPicker)
'    Call SendMessage(pDatePicker.hWnd, WM_SYSKEYDOWN, VK_DOWN, CLng(0))
'End Sub

Public Function Reinicio_Aplicacion(ByVal pProceso As String, _
ByVal pAplicacion As String, _
Optional ByVal pControlID As Boolean = False, _
Optional ByVal pIDProceso As Long, _
Optional ByVal pTiempoEsperaenMilisegundos As Long = 2000) As Boolean
    
    On Error GoTo Errores
    
    If DebugVBApp Then Exit Function
    ' Para que no me tumbe el Visual Studio si estoy debuggeando.
    ' Esta variable la lleno al inicio del programa.
    
    '---------------------------------------------'
    ' FUNCION PARA EL REINICIO DE UNA APLICACION  '
    '                                             '
    ' UTILIZA UN VBSCRIPT QUE SE CREA, SE EJECUTA '
    ' Y Espera A LA FINALIZACION DE LA APLICACION '
    ' PARA VOLVERLE A EJECUTAR                    '
    '---------------------------------------------'
    
    Dim TmpArchivo As String
    
    Dim TmpScript As Object
    Dim ObjCreateScript As Object
    
    TmpArchivo = App.Path & "\TmpRestartApplication.vbs"
    
    If (Dir(TmpArchivo) <> "") Then Kill (TmpArchivo)
    
    Set TmpScript = CreateObject("Scripting.FileSystemObject")
    
    Set ObjCreateScript = TmpScript.CreateTextFile(TmpArchivo, False)
    
    With ObjCreateScript
        
        .WriteLine "Option Explicit"
        .WriteLine "'-------------------------------------'"
        .WriteLine "' VBSCRIPT QUE REINICIA LA APLICACION '"
        .WriteLine "' '" & pAplicacion & "'        '"
        .WriteLine "' CUANDO ESTA SE CIERRA               '"
        .WriteLine "'-------------------------------------'"
        .WriteLine "'"
        .WriteLine "Const strComputer = ""."" "
        .WriteLine "'"
        .WriteLine "Dim objWMIService"
        .WriteLine "Dim colProcessList"
        .WriteLine "Dim bolL_Proceso_Terminado"
        .WriteLine "Dim objProcess"
        .WriteLine "Dim WshShell"
        .WriteLine "Dim oExec"
        .WriteLine "'"
        .WriteLine "Set objWMIService = GetObject(""winmgmts:"" & ""{impersonationLevel=impersonate}!\\"" & strComputer & ""\root\cimv2"")"
        .WriteLine "'"
        .WriteLine "Do"
        .WriteLine "    Set colProcessList = objWMIService.ExecQuery(""SELECT * FROM Win32_Process WHERE Name = '" & pProceso & "'"")"
        .WriteLine "    bolL_Proceso_Terminado = true"
        .WriteLine "    '"
        .WriteLine "    For Each objProcess in colProcessList"
        
        If (pControlID = False) Then
            .WriteLine "        WScript.Echo ""�ENCONTRADO EL PROCESO '" & pProceso & "' !"""
            .WriteLine "        bolL_Proceso_Terminado = True"
        Else
            .WriteLine "        If (objProcess.ProcessId = " & pIDProceso & ") Then"
            .WriteLine "            WScript.Echo ""�ENCONTRADO EL PROCESO '" & pProceso & "' ID:" & pIDProceso & " !"""
            .WriteLine "            bolL_Proceso_Terminado = True"
            .WriteLine "        End If"
        End If
        
        .WriteLine "        "
        .WriteLine "    Next"
        .WriteLine "    "
        .WriteLine "Loop While bolL_Proceso_Terminado = False"
        .WriteLine "'"
        .WriteLine "' EL PROCESO O TERMINO O NO EXIST�A"
        .WriteLine "' ENTONCES LO VOLVEMOS A EJECUTAR"
        .WriteLine "'"
        .WriteLine "Set WshShell = WScript.CreateObject(""WScript.Shell"")"
        
        If pControlID Then
            .WriteLine "Set oExec = WshShell.Exec(""TASKKILL /F /PID " & pIDProceso & """)"
        Else
            .WriteLine "Set oExec = WshShell.Exec(""TASKKILL /F /IM " & pProceso & """)"
        End If
        
        .WriteLine "WScript.Sleep " & pTiempoEsperaenMilisegundos & " ' NO SATUREMOS EL SISTEMA"
        .WriteLine "Set oExec = WshShell.Exec(""" & pAplicacion & """)"
        .WriteLine "'WScript.Echo ""�ARRANQUE DE LA APLICACION '" & pAplicacion & "' FINALIZADO!"""
        
        .Close
        
    End With
    
    Set ObjCreateScript = Nothing
    Set TmpScript = Nothing
    
    Shell "cscript """ & TmpArchivo & """"
    
    Reinicio_Aplicacion = True
    
    Exit Function
    
Errores:
    
    Err.Clear
    
End Function

Public Function ChangePriority(dwPriorityClass As Long) As Boolean

    On Error GoTo Err

    Dim hProcess&
    Dim Ret&, pID&
    
    pID = GetCurrentProcessId() ' get my proccess id
    
    ' get a handle to the process
    
    hProcess = OpenProcess(PROCESS_DUP_HANDLE, True, pID)
    
    If hProcess = 0 Then
        ChangePriority = False
        Exit Function
    End If

    Ret = SetPriorityClass(hProcess, dwPriorityClass)
    
    ' Close the source process handle
    
    Call CloseHandle(hProcess)
    
    If Ret = 0 Then
        ChangePriority = False
        Exit Function
    End If
    
    Exit Function
    
Err:
    
    ChangePriority = False
    
End Function

Sub EmptyComboBox(Combo As ComboBox)
    Dim Cont As Long
    For Cont = 0 To Combo.ListCount - 1
        Combo.RemoveItem 0
    Next Cont
End Sub

Public Sub PlaySoundFileAsync(pPath As String, Optional pLoop As Boolean = False)
    
    Dim TmpFlags, X
    
    If pLoop Then
        TmpFlags = (SND_FILENAME Or SND_ASYNC Or SND_LOOP)
    Else
        TmpFlags = (SND_FILENAME Or SND_ASYNC)
    End If

    X = PlaySound(pPath, 0, TmpFlags)
    
End Sub

Public Sub StopPlaySound()
    Call PlaySound(vbNullString, 0, SND_ASYNC)
End Sub

Public Function LocateParam(ByVal ParamIDString As String, _
ByRef pParamsArr() As String, Optional ByRef OutParamValue, _
Optional ByRef OutRawParam) As Boolean
    
    LocateParam = False
    
    Dim RawParam
    
    For Each RawParam In pParamsArr
        If UCase(RawParam) Like "*" & UCase(ParamIDString) & "*" Then
            If Left(UCase(LTrim(RawParam)), Len(ParamIDString)) = UCase(ParamIDString) Then
                
                LocateParam = True
                
                If Not IsMissing(OutParamValue) Then
                    OutParamValue = GetParamValue(ParamIDString, RawParam)
                End If
                
                If Not IsMissing(OutRawParam) Then
                    OutRawParam = RawParam
                End If
                
                Exit Function
                
            End If
        End If
    Next
    
End Function

Public Function GetParamValue(ByVal ParamIDString As String, ByVal pRawParam As String) As String
    
    Dim ParamIDLength As Long
    
    ParamIDLength = Len(ParamIDString)
    
    If Len(ParamIDString) < Len(LTrim(pRawParam)) Then
        GetParamValue = Strings.Mid(LTrim(pRawParam), Len(ParamIDString) + 1, Len(pRawParam) - Len(ParamIDString) + 1)
    Else
        GetParamValue = vbNullString
    End If
    
End Function

' Start the indicated program and wait for it
' to finish, hiding while we wait.

Public Function ShellAndWait(ByVal PathName As String, _
Optional ByVal WindowStyle As VbAppWinStyle = vbMinimizedFocus) As Double
    
    Dim Process_Id As Long
    Dim Process_Handle As Long
    
    Process_Id = Shell(PathName, WindowStyle)

    DoEvents

    ' Wait for the program to finish.
    ' Get the process handle.
    
    Process_Handle = OpenProcess(SYNCHRONIZE, 0, Process_Id)
    
    If Process_Handle <> 0 Then
        WaitForSingleObject Process_Handle, INFINITE
        CloseHandle Process_Handle
    End If
    
End Function

Public Sub WaitForProcess(ByVal ProcID As Long)
    
    On Error GoTo Error
    
    Dim Process_Id As Long: Process_Id = ProcID
    Dim Process_Handle As Long

    DoEvents

    ' Wait for the program to finish.
    ' Get the process handle.
    
    Process_Handle = OpenProcess(SYNCHRONIZE, 0, Process_Id)
    
    If Process_Handle <> 0 Then
        WaitForSingleObject Process_Handle, INFINITE
        CloseHandle Process_Handle
    End If
    
    Exit Sub
    
Error:
    
End Sub

Function SetDataReportZoom_On_150Percent(ByVal lhWnd As Long, ByVal lParam As Long) As Long
    
    On Error Resume Next
    
    Dim RetVal As Long
    Dim WinClassBuf As String * 255, WinTitleBuf As String * 255
    Dim WinClass As String, WinTitle As String
    Dim WinRect As Rect
    Dim WinWidth As Long, WinHeight As Long, ChildCount As Long
    
    RetVal = GetClassName(lhWnd, WinClassBuf, 255)
    WinClass = StripNulls(WinClassBuf)  ' remove extra Nulls & spaces
    RetVal = GetWindowText(lhWnd, WinTitleBuf, 255)
    WinTitle = StripNulls(WinTitleBuf)
    
    ChildCount = ChildCount + 1
    
    ' see the Windows Class and Title for each Child Window enumerated
    'Debug.Print "   Child Class = "; WinClass; ", Title = "; WinTitle
    ' You can find any type of Window by searching for its WinClass
    
    If Trim(UCase(WinClass)) = UCase("ComboBox") And WinTitle = "100%" Then
        SetFocus lhWnd
        
        CBSelectExact lhWnd, IIf(Len(WinTitle) = 3, "150%", "10%")
        
        SafeSendKeys "{F4}"
        SafeSendKeys "1"
        SafeSendKeys "{F4}"
    End If
    
    SetDataReportZoom_On_150Percent = True
    
End Function

Private Sub CBSelectExact(ByVal hwndCombo As Long, ByVal strSearch As String)
    
    On Error Resume Next
    
    Dim smResult As Long

    smResult = SendMessage(hwndCombo, CB_FINDSTRINGEXACT, 0, ByVal strSearch)
    
    If smResult <> CB_ERR Then ' Item is there
        smResult = SendMessage(hwndCombo, CB_SETCURSEL, smResult, CLng(0))
    End If

End Sub

Public Function StripNulls(OriginalStr As String) As String
    
    ' This removes the extra Nulls so String comparisons will work.
    
    On Error Resume Next
    
    If (InStr(OriginalStr, Chr(0)) > 0) Then
       OriginalStr = Left(OriginalStr, InStr(OriginalStr, Chr(0)) - 1)
    End If
    
    StripNulls = OriginalStr
    
End Function

Public Sub SafeSendKeys(ByVal Keys As String, Optional ByVal Wait)
    
    On Error GoTo SendKeysError
    
    If Not IsMissing(Wait) Then
        VBA.Interaction.SendKeys Keys, Wait
    Else
        VBA.Interaction.SendKeys Keys
    End If
    
    Exit Sub
    
SendKeysError:
    
    'Debug.Print err.Description ' Acceso denegado puede ocurrir...
    
    Resume IntentarOtraCosa
    
IntentarOtraCosa:
        
    On Error GoTo OtroError
    
    Dim WshShell As Object
    
    Set WshShell = CreateObject("wscript.shell")
    
    WshShell.SendKeys Keys, Wait
    
    Set WshShell = Nothing
    
    Exit Sub
    
OtroError:

    Resume UltimaOpcion

UltimaOpcion:
    
    On Error GoTo Olvidalo
    
    SendKeys Keys
    
Olvidalo:
    
    ' ???. Debug.Print err.Description. Prevenir error de runtime...
    
End Sub

Public Sub MoverVentana(hWnd As Long)
    
    On Error Resume Next
    
    Dim lngReturnValue As Long
    
    Call ReleaseCapture
    
    lngReturnValue = SendMessage( _
    hWnd, WM_NCLBUTTONDOWN, HTCAPTION, 0&)
    
End Sub

Public Function AllAppForms() As Collection
    
    Set AllAppForms = New Collection
    
    Dim i As Long
    
    For i = Forms.Count - 1 To 0 Step -1 ' Getting This App Forms in Call Stack Order
        AllAppForms.Add Forms(i)
    Next i
    
    If Not CallerAppForms Is Nothing Then
        For i = 1 To CallerAppForms.Count ' Getting Caller App Forms. They should be in Call Stack Order already if supplied.
            AllAppForms.Add CallerAppForms(i)
        Next i
    End If
    
End Function

Function TrayName() As String
    TrayName = App.ProductName & " v" & App.Major & "." & App.Minor & "." & App.Revision & vbCrLf & App.CompanyName & vbNullChar
End Function

Sub ToggleTaskbarVisibility(pForm As Form, ByVal pSetVisible As Boolean)
    
    Dim Style As Long
    
    Style = GetWindowLong(pForm.hWnd, GWL_EXSTYLE)
    
    If pSetVisible Then
        Style = Style Or WS_EX_APPWINDOW
    Else
        If Style And WS_EX_APPWINDOW Then
            Style = Style - WS_EX_APPWINDOW
        End If
    End If
    
    ShowWindow pForm.hWnd, 0
    SetWindowLong pForm.hWnd, GWL_EXSTYLE, Style
    ShowWindow pForm.hWnd, 4
    
End Sub

Public Sub TrayApp(Optional Trigger As Form)
    
    If Trigger Is Nothing Then Set Trigger = Screen.ActiveForm
    If Trigger Is Nothing Then Exit Sub
    
    Dim TmpForm As Form, TriggerHandle As Long, AllForms As Collection
    
    TriggerHandle = Trigger.hWnd
    
    If TrayedForms Is Nothing Then
        
        Set TrayedForms = New Collection
        
        Dim PrevLeft, PrevTop, _
        PrevWidth, PrevHeight, _
        PrevShownInTaskbar, i As Long
        
        Set AllForms = AllAppForms
        
        For i = 1 To AllForms.Count
            
            Set TmpForm = AllForms(i)
            
            PrevLeft = TmpForm.Left
            PrevTop = TmpForm.Top
            PrevWidth = TmpForm.Width
            PrevHeight = TmpForm.Height
            PrevShownInTaskbar = False
            
            Dim Style As Long
            
            Style = GetWindowLong(TmpForm.hWnd, GWL_EXSTYLE)
            
            If Style And WS_EX_APPWINDOW Then
                Style = Style - WS_EX_APPWINDOW
                PrevShownInTaskbar = True
            End If
            
            TrayedForms.Add Array(TmpForm, PrevLeft, PrevWidth, _
            PrevTop, PrevHeight, PrevShownInTaskbar), _
            TmpForm.Name & i
            
            'TmpForm.Visible = False ' No se puede en una App con ventanas modales.
            
            'TmpForm.Width = 1 ' Podr�a causar estragos en forms con eventos resize.
            'TmpForm.Height = 1
            
            TmpForm.Left = -100000
            TmpForm.Top = -100000 ' Esto es todo lo que necesitamos para "Ocultar" el app.
            
            If PrevShownInTaskbar Then
                ToggleTaskbarVisibility TmpForm, False
            End If
            
        Next i
        
    End If
    
    '------------------------
    '--- Create Tray Icon ---
    '------------------------
    
    TrayInfo.cbSize = Len(TrayInfo)
    TrayInfo.hWnd = TriggerHandle
    TrayInfo.uId = vbNull
    TrayInfo.uFlags = NIF_ICON Or NIF_TIP Or NIF_MESSAGE
    TrayInfo.uCallBackMessage = WM_MOUSEMOVE
    
    If Not CallerAppIcon Is Nothing Then
        TrayInfo.hIcon = CallerAppIcon
    Else
        TrayInfo.hIcon = FrmServerConnection.Picture
    End If
    
    ' Tip Text
    
    If CallerAppCaption <> vbNullString Then
        TrayInfo.szTip = CallerAppCaption
    Else
        TrayInfo.szTip = TrayName
    End If
    
    ' Adds the icon to the taskbar area.
    Shell_NotifyIcon NIM_ADD, TrayInfo
    
End Sub

Public Sub UnTrayApp()
    
    If TrayedForms Is Nothing Then Exit Sub
    
    Dim TmpForm As Form, i As Long

    For i = 1 To TrayedForms.Count
        
        Set TmpForm = TrayedForms(i)(0)
        
        If TrayedForms(i)(5) Then
            ToggleTaskbarVisibility TmpForm, True
            WindowFlashA TmpForm.hWnd, , 250, 2
        End If
        
        TmpForm.Left = TrayedForms(i)(1)
        
        TmpForm.Top = TrayedForms(i)(3)
        
    Next
    
    Set TrayedForms = Nothing
    
    'Deletes the tray icon when the form is shown
    Shell_NotifyIcon NIM_DELETE, TrayInfo ' del tray icon
    
End Sub

'Purpose     :  Causes the specified window to "flash".
'Inputs      :  lhwndWindow         The handle to the window to flash.
'               [lFlags]            One or a combinate of the "FLASHW_" flags listed in the declaration section above
'               [lFlashRate]        If specified is the rate at which the screen flashes in ms,
'                                   else uses the default.
'               [lNumFlashes]       The number of times to flash the window. The default, zero, causes it to flash indefinitely.
'Outputs     :  Returns True if the window was active before the call else returns False.
'OS          :  Windows 2000; Windows 98
'Notes       :  Flashing a window means changing the appearance of its caption bar as if the window were changing from
'               inactive to active status, or vice versa. (An inactive caption bar changes to an active caption bar;
'               an active caption bar changes to an inactive caption bar.)

Function WindowFlashA(lhwndWindow As Long, _
Optional lFlags As Long = FLASHW_ALL Or FLASHW_TIMER, _
Optional lFlashRate As Long = 0, _
Optional lNumFlashes As Long = 0) As Boolean
    
    Dim tFlash As FlashWInfo
    
    On Error GoTo ErrFailed
    
    tFlash.cbSize = Len(tFlash)
    'Specifies the flash status
    tFlash.dwFlags = lFlags
    'Specifies the rate, in milliseconds, at which the window will be flashed.
    'If dwTimeout is zero, the function uses the default cursor blink rate.
    tFlash.dwTimeout = lFlashRate
    'Handle to the window to be flashed. The window can be either opened or minimized.
    tFlash.hWnd = lhwndWindow
    'Specifies the number of times to flash the window. Zero will cause it to flash indefinately.
    tFlash.uCount = lNumFlashes
    'Call the API to flash the window
    WindowFlashA = FlashWindowEx(tFlash)
    
    Exit Function
    
ErrFailed:
    
    Debug.Print "Error in WindowFlashA: " & Err.Description
    WindowFlashA = False
    
End Function

Public Function GetProcessInfoType(pID As Long) As ProcessEntry32
    
    On Error GoTo Error
    
    Dim hSnapShot As Long
    Dim uProcess As ProcessEntry32
    Dim Success As Long
    
    hSnapShot = CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, 0&)
    
    If hSnapShot = -1 Then Err.Raise 999
    
    uProcess.dwSize = Len(uProcess)
    
    Success = ProcessFirst(hSnapShot, uProcess)
    
    If Success = 1 Then
        Do
            If uProcess.th32ProcessID = pID Then
                GetProcessInfoType = uProcess
                Exit Do
            End If
        Loop While ProcessNext(hSnapShot, uProcess)
    End If
    
    Call CloseHandle(hSnapShot)
    
    Exit Function
    
Error:
    
    GetProcessInfoType.th32ProcessID = 0
    
End Function

Public Function GetProcessInfo(pID As Long) As Boolean
    AppProcInfo = GetProcessInfoType(pID)
    GetProcessInfo = AppProcInfo.th32ProcessID > 0
End Function
